import React from "react";
import { ACTIONS } from "./App";
import Checkbox from "./components/editor/Checkbox";
import { FaCaretDown } from "react-icons/fa";
import {
  createRulesXML,
  rulesXSLTransformation,
  createPredefinedGrammar,
  createValidationXML,
  validationXSLTValidatorSection,
  validationXSLTValidationRegistrySection,
  validationXSLTImportSection,
  sortRulesToIsRootFirst,
  generatorTsXSLTransformation,
  indexTsXSLTransformation,
} from "./XMLUtils";

const Sidebar = ({ getRules, checkboxData, state, dispatch }) => {
  function showEditor(id) {
    dispatch({
      type: ACTIONS.SET_EDITOR_VISIBILITY,
      payload: { id: id, value: true },
    });
  }

  function createNewRule() {
    dispatch({ type: ACTIONS.INCREMENT_COUNT });
    dispatch({ type: ACTIONS.ADD_EDITOR });
  }

  return (
    <>
      <nav className="sidebar">
        <div className="text">Actions</div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginBottom: "10px",
          }}
        >
          <button
            type="button"
            className="btn btn-dark sidebar-action-button"
            onClick={() => createNewRule()}
          >
            New rule
          </button>
          <button
            type="button"
            className="btn btn-dark sidebar-action-button"
            onClick={() => {
              const result = window.ipcRenderer.invoke("electron-open-dialog");
              result
                .then((val) => (val.canceled ? "canceled" : val))
                .then((res) =>
                  res === "canceled" ? res : JSON.parse(res.data)
                )
                .then((stateData) =>
                  stateData === "canceled"
                    ? stateData
                    : dispatch({
                        type: ACTIONS.SET_STATE,
                        payload: { value: stateData },
                      })
                )
                .catch(alert);
            }}
          >
            Import project
          </button>
          <button
            type="button"
            className="btn btn-dark sidebar-action-button"
            onClick={async () => {
              const appStateJsonFormat = JSON.stringify(state);
              await window.ipcRenderer.invoke(
                "electron-save-dialog",
                appStateJsonFormat
              );
            }}
          >
            Save project
          </button>
          <button
            type="button"
            className="btn btn-dark sidebar-action-button"
            onClick={() => {
              const rules = getRules();

              // Check if there is a root
              const rootArr = Object.values(rules)
                .map((obj) =>
                  obj.isRoot ? state.editors[obj.id].rulename : null
                )
                .filter((val) => val !== null);
              if (rootArr.length === 0) {
                const type = "warning";
                const title = "Root";
                const message =
                  `Please select a rule to be root.`;
                window.ipcRenderer.send(
                  "electron-dialog",
                  type,
                  title,
                  message
                );
                return;
              } else if (rootArr.length > 1) {
                const type = "warning";
                const title = "Root";
                const message =
                  `Please make sure there is only one rule set to be root.
                  \nCurrent rules set to be root: ${rootArr}.`;
                window.ipcRenderer.send(
                  "electron-dialog",
                  type,
                  title,
                  message
                );
                return;
              }

              const rulesXMLContent = createRulesXML(state.editors, rules);
              const predefinedGrammar = createPredefinedGrammar(
                state.editors,
                rules
              );
              //console.log("Predefined grammar:", predefinedGrammar);
              const rulesXSLTResult = rulesXSLTransformation(
                rulesXMLContent,
                predefinedGrammar
              );

              const validationXmlNoDuplicateRulenames = createValidationXML(
                state.editors,
                rules,
                false
              );
              const validationXmlContent = createValidationXML(
                state.editors,
                rules
              );
              //console.log("VALIDATION XML:", validationXmlContent);

              // Divide validation XSLT into three parts:
              // import
              // LangiumNameTestValidationRegistry
              // LangiumNameTestValidator

              // Import requires that there is no duplicate rulenames
              const importSection = validationXSLTImportSection(
                validationXmlNoDuplicateRulenames
              );
              const validationRegistrySection =
                validationXSLTValidationRegistrySection(validationXmlContent);
              const validatorSection =
                validationXSLTValidatorSection(validationXmlContent);
              const validationFile =
                importSection + validationRegistrySection + validatorSection;
              //console.log("Validation file:", validationFile);

              // Save grammar
              window.ipcRenderer.send("save_grammar", rulesXSLTResult);

              // Save validations
              window.ipcRenderer.send("save_validations", validationFile);

              const sortedRules = sortRulesToIsRootFirst(rules);
              if (sortedRules.length > 0) {
                // Use first rule. Create generator.ts and index.ts
                const rootRule = sortedRules[0];
                const rootRuleXmlContent = createRulesXML(state.editors, [
                  rootRule,
                ]);
                const generatorTsXSLTResult =
                  generatorTsXSLTransformation(rootRuleXmlContent);

                window.ipcRenderer.send(
                  "save_generator.ts",
                  generatorTsXSLTResult
                );

                const indexTsXSLTResult =
                  indexTsXSLTransformation(rootRuleXmlContent);

                window.ipcRenderer.send("save_index.ts", indexTsXSLTResult);
              }

              // Generate langium grammar
              window.ipcRenderer.send("generate_langium_grammar");

              // Run langium
              window.ipcRenderer.send("run_langium");
            }}
          >
            Generate DSL (Press F5 to run)
          </button>
        </div>
        <ul>
          <li>
            <a href="#" className="collapsible">
              Editors
              <span>
                <FaCaretDown />
              </span>
            </a>
            <ul className="content">
              {Object.values(state.editors).map((editor) => (
                <li key={editor.id} title={editor.rulename}>
                  <a href="#" onClick={() => showEditor(editor.id)}>
                    {editor.rulename}
                  </a>
                </li>
              ))}
            </ul>
          </li>

          <li>
            <a href="#" className="collapsible">
              Layers
              <span>
                <FaCaretDown />
              </span>
            </a>
            <ul className="content">
              {checkboxData.map((data) => (
                <li key={data.label}>
                  <a href="#">
                    <Checkbox
                      label={data.label}
                      value={data.value}
                      onChange={data.onChange}
                    />
                  </a>
                </li>
              ))}
            </ul>
          </li>
        </ul>
      </nav>
    </>
  );
};

export default Sidebar;
