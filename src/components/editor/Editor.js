import React from "react";
import PropTypes from "prop-types";
import { Col, Row } from "react-bootstrap";
import MonacoEditor from "react-monaco-editor";
import Form from "react-bootstrap/Form";
import { BsDashLg } from "react-icons/bs";

import { setEditorLanguage, addNewEditorRightClickAction } from "./EditorUtils";
import { addDecorations, removeAllDecorations } from "./EditorDecorations";
import MyDropdown from "./MyDropdown";
import { ACTIONS, CARDINALITY } from "./../../App.js";
import {
  isKeyword,
  isIdentifier,
  isNumber,
  isString,
  getAlternatives,
  getVariableName,
  getCrossReference,
  getRuleReference,
  getCardinality,
  getValidations,
  isValidation,
  rulenameExists,
} from "../../AppStateUtil.js";
import { VALIDATIONS } from "./data/Validations";
import { ValidationModal } from "../../ValidationModal";

class Editor extends React.Component {
  constructor(props) {
    super(props);
    this.decorations = [];
    this.state = {
      isKeywordChecked: false,
      isIdentifierChecked: false,
      isNumberChecked: false,
      isStringChecked: false,
      isAlternativesChecked: false,
      isVariableNameChecked: false,
      isCrossReferenceChecked: false,
      isRuleReferenceChecked: false,
      isCardinalityChecked: false,
      isValidationChecked: false,

      // Validation modal
      showValidationModal: false,
    };

    setEditorLanguage(
      this.props.languageName,
      this.getAllKeywords(),
      this.getAllIdentifiers()
    );
    this.updateDecorations();
  }

  handleCloseValidationModal = () => {
    console.log("Close modal");
    this.setState({
      showValidationModal: false,
    });
  };

  handleShowValidationModal = () => {
    console.log("Show modal");
    this.setState({
      showValidationModal: true,
    });
  };

  toggleKeywordChecked = () => {
    this.setState({
      isKeywordChecked: !this.state.isKeywordChecked,
    });
  };

  toggleIdentifierChecked = () => {
    this.setState({
      isIdentifierChecked: !this.state.isIdentifierChecked,
    });
  };

  toggleNumberChecked = () => {
    this.setState({
      isNumberChecked: !this.state.isNumberChecked,
    });
  };

  toggleStringChecked = () => {
    this.setState({
      isStringChecked: !this.state.isStringChecked,
    });
  };

  toggleAlternativesChecked = () => {
    this.setState({
      isAlternativesChecked: !this.state.isAlternativesChecked,
    });
  };

  toggleVariableNameChecked = () => {
    this.setState({
      isVariableNameChecked: !this.state.isVariableNameChecked,
    });
  };

  toggleCrossReferenceChecked = () => {
    this.setState({
      isCrossReferenceChecked: !this.state.isCrossReferenceChecked,
    });
  };

  toggleRuleReferenceChecked = () => {
    this.setState({
      isRuleReferenceChecked: !this.state.isRuleReferenceChecked,
    });
  };

  toggleCardinalityChecked = () => {
    this.setState({
      isCardinalityChecked: !this.state.isCardinalityChecked,
    });
  };

  toggleValidationChecked = () => {
    this.setState({
      isValidationChecked: !this.state.isValidationChecked,
    });
  };

  componentDidUpdate(prevProps) {
    setEditorLanguage(
      this.props.languageName,
      this.getAllKeywords(),
      this.getAllIdentifiers()
    );
    this.updateDecorations();
  }

  addNewKeyword = (keyword) => {
    if (!isKeyword(this.props.state, keyword)) {
      this.props.dispatch({
        type: ACTIONS.ADD_KEYWORD,
        payload: { id: this.props.id, key: keyword },
      });
    }
  };

  removeKeyword = (word) => {
    if (isKeyword(this.props.state, word)) {
      this.props.dispatch({
        type: ACTIONS.REMOVE_KEYWORD,
        payload: { id: this.props.id, key: word },
      });
    }
  };

  addNumber = (number) => {
    this.props.dispatch({
      type: ACTIONS.ADD_NUMBER,
      payload: { id: this.props.id, key: number },
    });
  };

  removeNumber = (numberToRemove) => {
    this.props.dispatch({
      type: ACTIONS.REMOVE_NUMBER,
      payload: { id: this.props.id, key: numberToRemove },
    });
  };

  addString = (word) => {
    if (!isString(this.props.state, word)) {
      this.props.dispatch({
        type: ACTIONS.ADD_STRING,
        payload: { id: this.props.id, key: word },
      });
    }
  };

  removeString = (word) => {
    if (isString(this.props.state, word)) {
      this.props.dispatch({
        type: ACTIONS.REMOVE_STRING,
        payload: { id: this.props.id, key: word },
      });
    }
  };

  addNewIdentifier = (word) => {
    if (!isIdentifier(this.props.state, word)) {
      this.props.dispatch({
        type: ACTIONS.ADD_IDENTIFIER,
        payload: { id: this.props.id, key: word },
      });
    }
  };

  removeIdentifier = (word) => {
    if (isIdentifier(this.props.state, word)) {
      this.props.dispatch({
        type: ACTIONS.REMOVE_IDENTIFIER,
        payload: { id: this.props.id, key: word },
      });
    }
  };

  handleKeywordClick = (word) => {
    isKeyword(this.props.state, word)
      ? this.removeKeyword(word)
      : this.addNewKeyword(word);
  };

  handleIdentifierClick = (word) => {
    isIdentifier(this.props.state, word)
      ? this.removeIdentifier(word)
      : this.addNewIdentifier(word);
  };

  handleNumberClick = (number) => {
    isNumber(this.props.state, number)
      ? this.removeNumber(number)
      : this.addNumber(number);
  };

  handleStringClick = (word) => {
    isString(this.props.state, word)
      ? this.removeString(word)
      : this.addString(word);
  };

  setAlternatives(word, newAlternatives) {
    this.props.dispatch({
      type: ACTIONS.SET_ALTERNATIVE,
      payload: {
        id: this.props.id,
        key: word,
        value: newAlternatives,
      },
    });
  }

  removeAlternatives(word) {
    this.props.dispatch({
      type: ACTIONS.REMOVE_ALTERNATIVE,
      payload: { id: this.props.id, key: word },
    });
  }

  /**
   * Gets called when monaco editor detects a change.
   *
   * @param {string} newValue
   */
  onChange = (newValue) => {
    this.props.dispatch({
      type: ACTIONS.UPDATE_EDITOR_CODE,
      payload: { id: this.props.id, value: newValue },
    });
  };

  componentDidMount() {
    this.updateDecorations();

    const model = this.editor.getModel();
    const temporaryClassName = "temporaryClass";

    const app = document.getElementById("app-screen");
    app.addEventListener("mouseup", function () {
      // To prevent a bug where the user clicks on a *word* and then drags it outside of editor.
      // If the user were to drag from outside any editor and then onto a word in an editor,
      // it would use the previously clicked *word*.
      const list = document.getElementsByClassName(temporaryClassName);
      for (const el of list) {
        el.remove();
      }
    });

    this.editor.onMouseDown((event) => {
      const mouseDownWordInfo = this.getWordInfoAtClick(
        event.target.position,
        model
      );

      if (mouseDownWordInfo == null) {
        return;
      }

      // Store info about word globally, so that it can be compared in onMouseUp event
      window.mouseDownWordInfo = mouseDownWordInfo;

      const word = mouseDownWordInfo.word;
      if (word == null) {
        return;
      }
      // Create new div element containing info about the clicked word
      const newEl = document.createElement("div");
      newEl.className = temporaryClassName;
      newEl.dataset.word = word;
      newEl.innerText = word;
      newEl.hidden = true;
      if (isKeyword(this.props.state, word)) {
        newEl.dataset.isKeyword = "true";
      }
      if (isIdentifier(this.props.state, word)) {
        newEl.dataset.isIdentifier = "true";
      }
      if (isNumber(this.props.state, word)) {
        newEl.dataset.isNumber = "true";
      }
      if (isString(this.props.state, word)) {
        newEl.dataset.isString = "true";
      }
      const alternatives = getAlternatives(this.props.state, word);
      if (alternatives) {
        newEl.dataset.alternatives = `${alternatives}`;
      }
      const variableName = getVariableName(this.props.state, word);
      if (variableName) {
        newEl.dataset.variableName = variableName;
      }
      const crossReference = getCrossReference(this.props.state, word);
      if (crossReference) {
        newEl.dataset.crossReference = crossReference;
      }
      const ruleReference = getRuleReference(this.props.state, word);
      if (ruleReference) {
        newEl.dataset.ruleReference = ruleReference;
      }
      const cardinality = getCardinality(this.props.state, word);
      if (cardinality) {
        newEl.dataset.cardinality = cardinality;
      }
      const validations = getValidations(this.props.state, word);
      if (validations) {
        const validationsJSON = JSON.stringify(validations);
        newEl.dataset.validations = validationsJSON;
      }

      // Add div to page so that it can be accessed in onMouseUp function
      document.getElementById("app-screen").append(newEl);
    });

    this.editor.onMouseUp((event) => {
      const mouseUpWordInfo = this.getWordInfoAtClick(
        event.target.position,
        model
      );
      if (mouseUpWordInfo == null) {
        return;
      }
      const word = mouseUpWordInfo.word;
      if (word == null) {
        return;
      }
      // Get element created in onMouseDown function
      const list = document.getElementsByClassName(temporaryClassName);
      for (const elem of list) {
        //console.log("Word mouse down:", window.mouseDownWordInfo);
        //console.log("Word mouse up: ", mouseUpWordInfo);

        if (this.isWordInfoEqual(window.mouseDownWordInfo, mouseUpWordInfo)) {
          //console.log("They are the same!");
          return;
        }

        this.props.setModalWordToCopyState((prev) => ({
          ...prev,
          wordFrom: window.mouseDownWordInfo.word,
          wordTo: mouseUpWordInfo.word,
          id: mouseUpWordInfo.id,
          keyword: false,
          identifier: false,
          number: false,
          string: false,
          alternatives: "",
          variableName: "",
          crossReference: "",
          ruleReference: "",
          cardinality: "",
          validations: [],
        }));

        if (elem.dataset.isKeyword) {
          this.props.setModalWordToCopyState((prev) => ({
            ...prev,
            keyword: true,
          }));
        }
        if (elem.dataset.isIdentifier) {
          this.props.setModalWordToCopyState((prev) => ({
            ...prev,
            identifier: true,
          }));
        }
        if (elem.dataset.isNumber) {
          this.props.setModalWordToCopyState((prev) => ({
            ...prev,
            number: true,
          }));
        }
        if (elem.dataset.isString) {
          this.props.setModalWordToCopyState((prev) => ({
            ...prev,
            string: true,
          }));
        }
        if (elem.dataset.alternatives) {
          this.props.setModalWordToCopyState((prev) => ({
            ...prev,
            alternatives: elem.dataset.alternatives,
          }));
        }
        if (elem.dataset.variableName) {
          this.props.setModalWordToCopyState((prev) => ({
            ...prev,
            variableName: elem.dataset.variableName,
          }));
        }
        if (elem.dataset.crossReference) {
          this.props.setModalWordToCopyState((prev) => ({
            ...prev,
            crossReference: elem.dataset.crossReference,
          }));
        }
        if (elem.dataset.ruleReference) {
          this.props.setModalWordToCopyState((prev) => ({
            ...prev,
            ruleReference: elem.dataset.ruleReference,
          }));
        }
        if (elem.dataset.cardinality) {
          this.props.setModalWordToCopyState((prev) => ({
            ...prev,
            cardinality: elem.dataset.cardinality,
          }));
        }
        if (elem.dataset.validations) {
          const validationsObj = JSON.parse(elem.dataset.validations);
          this.props.setModalWordToCopyState((prev) => ({
            ...prev,
            validations: validationsObj,
          }));
        }

        if (Object.keys(elem.dataset).length <= 1) {
          return;
        }

        // Show popup where the user can choose which properties to copy.
        this.props.handleShowModal();
      }
    });
  }

  isWordInfoEqual(downInfo, upInfo) {
    return (
      downInfo.word === upInfo.word &&
      downInfo.startColumn === upInfo.startColumn &&
      downInfo.endColumn === upInfo.endColumn &&
      downInfo.id === upInfo.id
    );
  }

  getWordInfoAtClick(position, model) {
    if (position == null || model == null) {
      return null;
    }
    let wordClickedInfo = model.getWordAtPosition(position);
    if (wordClickedInfo == null) {
      return null;
    }
    wordClickedInfo = { ...wordClickedInfo, id: this.props.id };
    return wordClickedInfo;
  }

  getAllWordsInCode() {
    return this.props.state.code.split(/[\s]+/);
  }

  getAllKeywords() {
    const allTokensThatAreKeywords = this.props.state.tokens.filter(
      (token) => token?.isKeyword
    );
    return allTokensThatAreKeywords.map((token) => token.word);
  }

  getAllIdentifiers() {
    const allTokensThatAreIdentifiers = this.props.state.tokens.filter(
      (token) => token?.isIdentifier
    );
    return allTokensThatAreIdentifiers.map((token) => token.word);
  }

  getAllNumbers() {
    const allTokensThatAreNumbers = this.props.state.tokens.filter(
      (token) => token?.isNumber
    );
    return allTokensThatAreNumbers.map((token) => token.word);
  }

  getAllStrings() {
    const allTokensThatAreStrings = this.props.state.tokens.filter(
      (token) => token?.isString
    );
    return allTokensThatAreStrings.map((token) => token.word);
  }

  editorDidMount = (editor, monaco) => {
    this.editor = editor;
    editor.focus();

    let editors = window.editors;
    editors = [...editors, editor];
    window.editors = editors;

    this.addEditorRightClickActions(editor);
  };

  addEditorRightClickActions(editor) {
    addNewEditorRightClickAction(
      editor,
      "Toggle Keyword",
      this.toggleKeywordForSelectedWord
    );
    //addNewEditorRightClickAction(editor, "Remove keyword", this.removeKeyword);

    addNewEditorRightClickAction(
      editor,
      "Toggle Identifier",
      this.toggleIdentifierForSelectedWord
    );

    addNewEditorRightClickAction(
      editor,
      "Toggle number",
      this.toggleNumberForSelectedWord
    );

    addNewEditorRightClickAction(
      editor,
      "Toggle string",
      this.toggleStringForSelectedWord
    );

    addNewEditorRightClickAction(
      editor,
      "Choose alternatives",
      this.chooseAlternativesForSelectedWord
    );

    addNewEditorRightClickAction(
      editor,
      "Choose variable name",
      this.chooseVariableNameForSelectedWord
    );

    addNewEditorRightClickAction(
      editor,
      "Choose cross reference",
      this.chooseCrossReferenceForSelectedWord
    );

    addNewEditorRightClickAction(
      editor,
      "Choose rule reference",
      this.chooseRuleReferenceForSelectedWord
    );

    addNewEditorRightClickAction(
      editor,
      "Choose cardinality",
      this.chooseCardinalityForSelectedWord
    );
    addNewEditorRightClickAction(
      editor,
      "Choose validation",
      this.handleShowValidationModal
    );
  }

  rulenameInputComponent() {
    return (
      <Form
        className="no-interactjs-action"
        onSubmit={async (event) => {
          event.preventDefault();
          const rulename = event.target.elements.rulename.value;

          if (
            rulenameExists(
              this.props.getAllEditorsInfo(),
              rulename,
              this.props.id
            )
          ) {
            // Show a popup telling the user that the rulename is already in use.
            const type = "warning";
            const title = "Rulename aleady in use";
            const message =
              "Please choose a rulename that is not already in use.";
            window.ipcRenderer.send("electron-dialog", type, title, message);
          } else if (rulename[0].toUpperCase() !== rulename[0]) {
            const type = "warning";
            const title = "Rulename";
            const message = "Please make rulename start with a capital letter.";
            window.ipcRenderer.send("electron-dialog", type, title, message);
          } else {
            this.updateEditorRulename(rulename);
          }
        }}
      >
        <Form.Control
          size="sm"
          type={"text"}
          name="rulename"
          placeholder={this.props.rulename}
        />
      </Form>
    );
  }

  options = {
    selectOnLineNumbers: true,
    minimap: { enabled: false },
  };

  // Update the parent of this editor. References the id of the parent editor.
  updateParent(parentID) {
    return this.props.dispatch({
      type: ACTIONS.UPDATE_EDITOR_PARENT,
      payload: { id: this.props.id, value: parentID },
    });
  }

  updateEditorRulename(rulename) {
    return this.props.dispatch({
      type: ACTIONS.UPDATE_EDITOR_RULENAME,
      payload: { id: this.props.id, value: rulename },
    });
  }

  closeEditor() {
    this.props.dispatch({
      type: ACTIONS.SET_EDITOR_VISIBILITY,
      payload: { id: this.props.id, value: false },
    });
    this.editor.dispose();
  }

  async removeEditor() {
    let options = {
      title: "Delete rule",
      buttons: ["Yes", "No"],
      message: "Are you sure you want to delete this rule?",
    };
    const response = await window.ipcRenderer.invoke(
      "electron-confirmation-dialog",
      options
    );
    if (response.response === 0) {
      this.props.dispatch({
        type: ACTIONS.REMOVE_ALL_RULE_REFERENCES_TO_EDITOR,
        payload: { id: this.props.id },
      });
      this.props.dispatch({
        type: ACTIONS.REMOVE_EDITOR,
        payload: { id: this.props.id },
      });
      this.editor.dispose();
    }
  }

  removeCrossReference(word) {
    this.props.dispatch({
      type: ACTIONS.REMOVE_CROSS_REFERENCE,
      payload: { id: this.props.id, key: word },
    });
  }

  setCrossReference(word, crossReference) {
    this.props.dispatch({
      type: ACTIONS.SET_CROSS_REFERENCE,
      payload: {
        id: this.props.id,
        key: word,
        value: crossReference,
      },
    });
  }

  setRuleReference(word, ruleReference) {
    this.props.dispatch({
      type: ACTIONS.SET_RULE_REFERENCE,
      payload: {
        id: this.props.id,
        key: word,
        value: ruleReference,
      },
    });
  }

  removeRuleReference(word) {
    this.props.dispatch({
      type: ACTIONS.REMOVE_RULE_REFERENCE,
      payload: { id: this.props.id, key: word },
    });
  }

  /**
   *
   * @param {String} word
   * @param {String} cardinality
   */
  setCardinality(word, cardinality) {
    this.props.dispatch({
      type: ACTIONS.SET_CARDINALITY,
      payload: { id: this.props.id, key: word, value: cardinality },
    });
  }

  addValidation(word, validationName, fieldsObj) {
    this.props.dispatch({
      type: ACTIONS.ADD_VALIDATION,
      payload: {
        id: this.props.id,
        key: word,
        value: validationName,
        fieldsObj: fieldsObj,
      },
    });
  }

  removeValidation = (word, validationName) => {
    this?.props?.dispatch({
      type: ACTIONS.REMOVE_VALIDATION,
      payload: { id: this.props.id, key: word, value: validationName },
    });
  };

  setVariableName = (word, variableName) => {
    this.props.dispatch({
      type: ACTIONS.SET_VARIABLE_NAME,
      payload: { id: this.props.id, key: word, value: variableName },
    });
  };

  toggleKeywordForSelectedWord = () => {
    const word = this.getSelectedWord();
    if (word == null) {
      return;
    }
    this.handleKeywordClick(word);
  };

  toggleIdentifierForSelectedWord = () => {
    const word = this.getSelectedWord();
    if (word == null) {
      return;
    }
    this.handleIdentifierClick(word);
  };

  toggleNumberForSelectedWord = () => {
    const word = this.getSelectedWord();
    if (word == null) {
      return;
    }
    this.handleNumberClick(word);
  };

  toggleStringForSelectedWord = () => {
    const word = this.getSelectedWord();
    if (word == null) {
      return;
    }
    this.handleStringClick(word);
  };

  chooseAlternativesForSelectedWord = async () => {
    const word = this.getSelectedWord();
    if (word == null) {
      return;
    }
    const alternatives = await window.ipcRenderer.invoke(
      "electron-prompt-alternatives"
    );
    if (alternatives == null) {
      return;
    }

    if (alternatives === "") {
      this.removeAlternatives(word);
    } else {
      console.log(
        `Set alternatives for word: ${word}, alternatives: ${alternatives}`
      );
      this.setAlternatives(word, alternatives);
    }
  };

  chooseVariableNameForSelectedWord = async () => {
    const word = this.getSelectedWord();
    if (word == null) {
      return;
    }
    const variableName = await window.ipcRenderer.invoke(
      "electron-prompt-variable-name"
    );
    if (variableName == null) {
      return;
    }
    this.setVariableName(word, variableName);
  };

  getOtherEditors() {
    const editorsInfo = this.props.getAllEditorsInfo();
    let otherEditors = {};
    for (const [key, value] of Object.entries(editorsInfo)) {
      // Here we need to use == instead of ===
      if (key == this.props.id) {
        continue;
      }
      otherEditors = { ...otherEditors, [key]: value };
    }
    return otherEditors;
  }

  alertNoOtherEditors() {
    const type = "warning";
    const title = "Parent";
    const message = `There are no other rules. Please create a new rule and try again.`;
    window.ipcRenderer.send("electron-dialog", type, title, message);
  }

  chooseCrossReferenceForSelectedWord = async () => {
    const word = this.getSelectedWord();
    if (word == null) {
      return;
    }
    const otherEditors = this.getOtherEditors();
    if (Object.keys(otherEditors).length === 0) {
      this.alertNoOtherEditors();
      return;
    }
    const editorRulenames = this.getEditorRulenames(otherEditors);
    const chosenRulename = await window.ipcRenderer.invoke(
      "electron-prompt-choose-rulename",
      "Cross reference",
      editorRulenames
    );

    if (chosenRulename === "") {
      this.removeCrossReference(word);
      return;
    }

    if (!editorRulenames.includes(chosenRulename)) {
      console.error(`Please choose a rulename that exists`);
      return;
    }

    // User has chosen a rulename that exists
    for (const value of Object.values(otherEditors)) {
      if (value.rulename === chosenRulename) {
        this.setCrossReference(word, value.id);
      }
    }
  };

  chooseRuleReferenceForSelectedWord = async () => {
    const word = this.getSelectedWord();
    if (word == null) {
      return;
    }
    const otherEditors = this.getOtherEditors();
    if (Object.keys(otherEditors).length === 0) {
      this.alertNoOtherEditors();
      return;
    }
    const editorRulenames = this.getEditorRulenames(otherEditors);
    const chosenRulename = await window.ipcRenderer.invoke(
      "electron-prompt-choose-rulename",
      "Rule reference",
      editorRulenames
    );

    if (chosenRulename === "") {
      this.removeRuleReference(word);
      return;
    }

    if (!editorRulenames.includes(chosenRulename)) {
      console.error(`Please choose a rulename that exists`);
      return;
    }
    for (const value of Object.values(otherEditors)) {
      if (value.rulename === chosenRulename) {
        this.setRuleReference(word, value.id);
      }
    }
  };

  chooseCardinalityForSelectedWord = async () => {
    const word = this.getSelectedWord();
    if (word == null) {
      return;
    }

    const cardinality = await this.getCardinalityFromUser();
    if (cardinality === "cancel") {
      return;
    }
    this.setCardinality(word, cardinality);
  };

  addValidationForSelectedWord = (validationName, fieldsObj) => {
    const word = this.getSelectedWord();
    if (word == null) {
      return;
    }

    if (isValidation(this.props.state, word, validationName)) {
      console.log(`${validationName} is already a validation for ${word}!`);
    } else {
      this.addValidation(word, validationName, fieldsObj);
    }
  };

  getCardinalityFromUser = async () => {
    return window.ipcRenderer.invoke("electron-prompt-cardinality");
  };

  getEditorRulenames(editors) {
    let editorRulenames = [];
    for (const value of Object.values(editors)) {
      editorRulenames = [...editorRulenames, value.rulename];
    }
    return editorRulenames;
  }

  getSelectedWord() {
    const model = this.editor?.getModel();
    if (!model) {
      return;
    }
    const selection = this.editor.getSelection();
    const position = {
      column: selection.positionColumn,
      lineNumber: selection.positionLineNumber,
    };
    const wordInfo = this.getWordInfoAtClick(position, model);
    if (wordInfo == null) {
      return;
    }
    return wordInfo?.word;
  }

  updateDecorations() {
    if (!this.editor) {
      return;
    }

    const keywordOptions = {
      beforeContentClassName: "keyword token1",
    };

    const identifierOptions = {
      beforeContentClassName: "identifier token1",
    };

    const numberOptions = {
      beforeContentClassName: "number token1",
    };

    const stringOptions = {
      beforeContentClassName: "string token1",
    };

    this.decorations = removeAllDecorations(this.editor, this.decorations);
    this.addDecorationsForKeywords(keywordOptions);
    this.addDecorationsForIdentifiers(identifierOptions);
    this.addDecorationsForNumbers(numberOptions);
    this.addDecorationsForStrings(stringOptions);
    this.addDecorationsForAlternatives();
    this.addDecorationsForVariableNames();
    this.addDecorationsForCrossReferences();
    this.addDecorationsForRuleReferences();
    this.addDecorationsForCardinalities();
    this.addDecorationsForValidations();
  }

  addDecorationsForKeywords(keywordOptions) {
    if (
      !(
        this.state.isKeywordChecked ||
        this.props.editorMasterLayers.showAllKeywords
      )
    ) {
      return;
    }
    this.decorations = addDecorations(
      this.editor,
      this.getAllKeywords(),
      keywordOptions,
      this.decorations
    );
  }

  addDecorationsForIdentifiers(identifierOptions) {
    if (
      !(
        this.state.isIdentifierChecked ||
        this.props.editorMasterLayers.showAllIdentifiers
      )
    ) {
      return;
    }
    this.decorations = addDecorations(
      this.editor,
      this.getAllIdentifiers(),
      identifierOptions,
      this.decorations
    );
  }

  addDecorationsForNumbers(numberOptions) {
    if (
      !(
        this.state.isNumberChecked ||
        this.props.editorMasterLayers.showAllNumbers
      )
    ) {
      return;
    }
    this.decorations = addDecorations(
      this.editor,
      this.getAllNumbers(),
      numberOptions,
      this.decorations
    );
  }

  addDecorationsForStrings(stringOptions) {
    if (
      !(
        this.state.isStringChecked ||
        this.props.editorMasterLayers.showAllStrings
      )
    ) {
      return;
    }
    this.decorations = addDecorations(
      this.editor,
      this.getAllStrings(),
      stringOptions,
      this.decorations
    );
  }

  addDecorationsForAlternatives() {
    if (
      !(
        this.state.isAlternativesChecked ||
        this.props.editorMasterLayers.showAllAlternatives
      )
    ) {
      return;
    }
    const allWordsInCode = this.getAllWordsInCode();
    const uniqueWords = [...new Set(allWordsInCode)];
    for (const word of uniqueWords) {
      const alternatives = getAlternatives(this.props.state, word);
      if (alternatives) {
        const alternativesOptions = {
          before: {
            content: `[${alternatives}]`,
            inlineClassName: "token1",
          },
        };
        this.decorations = addDecorations(
          this.editor,
          [word],
          alternativesOptions,
          this.decorations
        );
      }
    }
  }

  addDecorationsForVariableNames() {
    if (
      !(
        this.state.isVariableNameChecked ||
        this.props.editorMasterLayers.showAllVariableNames
      )
    ) {
      return;
    }
    for (const token of this.props.state.tokens) {
      if (!token?.variableName) {
        continue;
      }
      const variableNameOptions = {
        before: {
          content: `[🏷️"${token.variableName}"]`,
          inlineClassName: "token1",
        },
      };
      this.decorations = addDecorations(
        this.editor,
        [token.word],
        variableNameOptions,
        this.decorations
      );
    }
  }

  addDecorationsForCrossReferences() {
    if (
      !(
        this.state.isCrossReferenceChecked ||
        this.props.editorMasterLayers.showAllCrossReferences
      )
    ) {
      return;
    }

    for (const token of this.props.state.tokens) {
      if (!token?.crossReference) {
        continue;
      }
      const allEditorsInfo = this.props.getAllEditorsInfo();
      const crossReferenceOptions = {
        before: {
          content: `[➡️${
            allEditorsInfo[token.crossReference]?.rulename
          }]`,
          inlineClassName: "token1",
        },
      };
      this.decorations = addDecorations(
        this.editor,
        [token.word],
        crossReferenceOptions,
        this.decorations
      );
    }
  }

  addDecorationsForRuleReferences() {
    if (
      !(
        this.state.isRuleReferenceChecked ||
        this.props.editorMasterLayers.showAllRuleReferences
      )
    ) {
      return;
    }

    for (const token of this.props.state.tokens) {
      if (!token?.ruleReference) {
        continue;
      }
      const allEditorsInfo = this.props.getAllEditorsInfo();
      const ruleReferenceOptions = {
        before: {
          content: `[🔗${allEditorsInfo[token.ruleReference]?.rulename}]`,
          inlineClassName: "token1",
        },
      };
      this.decorations = addDecorations(
        this.editor,
        [token.word],
        ruleReferenceOptions,
        this.decorations
      );
    }
  }

  addDecorationsForCardinalities() {
    if (
      !(
        this.state.isCardinalityChecked ||
        this.props.editorMasterLayers.showAllCardinalities
      )
    ) {
      return;
    }

    for (const token of this.props.state.tokens) {
      if (!token?.cardinality) {
        continue;
      }
      const cardinality = token.cardinality;

      let cardinalityOptions = null;
      if (cardinality === CARDINALITY.ONE) {
        cardinalityOptions = {
          beforeContentClassName: "token1 cardinality-one",
        };
      } else if (cardinality === CARDINALITY.ZERO_OR_ONE) {
        cardinalityOptions = {
          beforeContentClassName: "token1 cardinality-zero-or-one",
        };
      } else if (cardinality === CARDINALITY.ZERO_OR_MORE) {
        cardinalityOptions = {
          beforeContentClassName: "token1 cardinality-zero-or-more",
        };
      } else if (cardinality === CARDINALITY.ONE_OR_MORE) {
        cardinalityOptions = {
          beforeContentClassName: "token1 cardinality-one-or-more",
        };
      }

      if (cardinalityOptions == null) {
        return;
      }
      this.decorations = addDecorations(
        this.editor,
        [token.word],
        cardinalityOptions,
        this.decorations
      );
    }
  }

  addDecorationsForValidations() {
    if (
      !(
        this.state.isValidationChecked ||
        this.props.editorMasterLayers.showAllValidations
      )
    ) {
      return;
    }
    for (const token of this.props.state.tokens) {
      if (!token?.validations || token?.validations.length === 0) {
        continue;
      }

      let validationStr = "";
      if (token.validations.length > 1) {
        validationStr = "🚩"; // Emojis
      } else {
        validationStr = "🚩"; // Emojis
      }
      for (const [index, validation] of token.validations.entries()) {
        validationStr += `${validation?.name}`;
        const min = validation?.fields?.min;
        const max = validation?.fields?.max;
        const number = validation?.fields?.number;
        const value = validation?.fields?.value;
        // Assumes that min and max are not combined with number or value
        if (min && max) {
          validationStr += ` (${min}, ${max})`;
        } else if (min && !max) {
          validationStr += ` (${min})`;
        } else if (!min && max) {
          validationStr += ` (${max})`;
        }

        if (number) {
          // Assumes that number is not combined with value or min/max
          validationStr += ` (${number})`;
        }

        // Assumes that value is not combined with number or min/max
        if (value && token.isNumber) {
          validationStr += ` (${value})`;
        } else if (value && !token.isNumber) {
          validationStr += ` ('${value}')`;
        }

        if (index !== token.validations.length - 1) {
          // Separate each validation with a colon
          validationStr += ", ";
        }
      }

      const validationOptions = {
        before: {
          content: `[${validationStr}]`,
          inlineClassName: "token1",
        },
      };
      this.decorations = addDecorations(
        this.editor,
        [token.word],
        validationOptions,
        this.decorations
      );
    }
  }

  editorTopBar() {
    const layersCheckboxData = [
      {
        label: "Keyword",
        value: this.state.isKeywordChecked,
        onClick: this.toggleKeywordChecked,
      },
      {
        label: "Identifier",
        value: this.state.isIdentifierChecked,
        onClick: this.toggleIdentifierChecked,
      },
      {
        label: "Number",
        value: this.state.isNumberChecked,
        onClick: this.toggleNumberChecked,
      },
      {
        label: "String",
        value: this.state.isStringChecked,
        onClick: this.toggleStringChecked,
      },
      {
        label: "Alternatives",
        value: this.state.isAlternativesChecked,
        onClick: this.toggleAlternativesChecked,
      },
      {
        label: "Variable name",
        value: this.state.isVariableNameChecked,
        onClick: this.toggleVariableNameChecked,
      },
      {
        label: "Cross reference",
        value: this.state.isCrossReferenceChecked,
        onClick: this.toggleCrossReferenceChecked,
      },
      {
        label: "Rule reference",
        value: this.state.isRuleReferenceChecked,
        onClick: this.toggleRuleReferenceChecked,
      },
      {
        label: "Cardinality",
        value: this.state.isCardinalityChecked,
        onClick: this.toggleCardinalityChecked,
      },
      {
        label: "Validations",
        value: this.state.isValidationChecked,
        onClick: this.toggleValidationChecked,
      },
    ];

    const propertiesData = [
      {
        label: "Keyword 🔑",
        onClick: this.toggleKeywordForSelectedWord,
      },
      {
        label: "Identifier 🆔",
        onClick: this.toggleIdentifierForSelectedWord,
      },
      {
        label: "Number 🔢",
        onClick: this.toggleNumberForSelectedWord,
      },
      {
        label: "String 🔤",
        onClick: this.toggleStringForSelectedWord,
      },
      {
        label: "Alternatives",
        onClick: this.chooseAlternativesForSelectedWord,
      },
      {
        label: "Variable name 🏷️",
        onClick: this.chooseVariableNameForSelectedWord,
      },
      {
        label: "Cross reference ➡️",
        onClick: this.chooseCrossReferenceForSelectedWord,
      },
      {
        label: "Rule reference 🔗",
        onClick: this.chooseRuleReferenceForSelectedWord,
      },
      {
        label: "Cardinality",
        onClick: this.chooseCardinalityForSelectedWord,
      },
    ];

    const validationData = Object.values(VALIDATIONS).map(
      (validation) => validation
    );

    const ParentButton = (
      <div
        type="button"
        className="btn btn-success btn-small"
        onClick={async () => {
          const otherEditors = this.getOtherEditors();
          if (Object.keys(otherEditors).length === 0) {
            this.alertNoOtherEditors();
            return;
          }
          const parentRulename = this.props.getEditorRulename(
            this.props.state.parentID
          );
          const title = "Choose parent";
          const label =
            parentRulename == null
              ? `Current parent: --- No reference ---`
              : `Current parent: ${parentRulename}`;
          let parentID = await window.ipcRenderer.invoke(
            "electron-prompt-choose-parent",
            title,
            label,
            otherEditors
          );
          if (parentID !== null) {
            if (parentID === "") {
              parentID = -1;
            } else {
              parentID = parseInt(parentID);
            }
            this.updateParent(parentID);
          }
        }}
      >
        {this.props.getEditorRulename(this.props.state.parentID) || "Parent"}
      </div>
    );
    const RootCheckbox = (
      <div className="no-interactjs-action" style={{ paddingRight: "20px" }}>
        <label>
          <input
            type="checkbox"
            checked={this.props.state.isRoot}
            onChange={() =>
              this.props.dispatch({
                type: ACTIONS.SET_EDITOR_IS_ROOT,
                payload: {
                  id: this.props.id,
                  value: !this.props.state.isRoot,
                },
              })
            }
          />
          root
        </label>
      </div>
    );
    return (
      <Row className="flex-nowrap" style={{ alignItems: "center" }}>
        <button
          style={{
            marginLeft: "20px",
            backgroundColor: "red",
            color: "red",
            opacity: 0.75,
            borderRadius: "100%",
          }}
          type="button"
          className="btn-close btn-danger btn-sm"
          aria-label="Remove"
          onClick={() => this.removeEditor()}
        ></button>
        <button
          style={{
            marginLeft: "5px",
            backgroundColor: "yellow",
            borderRadius: "100%",
          }}
          className="btn-bs btn-warning btn-sm"
          type="button"
          aria-label="Minimize"
          onClick={() => this.closeEditor()}
        >
          <BsDashLg style={{ display: "flex", alignItems: "center" }} />
        </button>
        <Col style={{ maxWidth: "300px", minWidth: "35%" }}>
          {this.rulenameInputComponent()}
        </Col>
        <div className="btn-group">
          {RootCheckbox}
          {ParentButton}
          <MyDropdown
            className="no-interactjs-action"
            title="Layers"
            data={layersCheckboxData}
            isCheckbox={true}
          />
          <MyDropdown
            className="no-interactjs-action"
            title="Properties"
            data={propertiesData}
          />
          <ValidationModal
            show={this.state.showValidationModal}
            handleShow={this.handleShowValidationModal}
            handleClose={this.handleCloseValidationModal}
            validationData={validationData}
            selectedWord={this.getSelectedWord()}
            addValidationForSelectedWord={this.addValidationForSelectedWord}
            removeValidation={this.removeValidation}
            editorState={this.props.state}
          ></ValidationModal>
        </div>
      </Row>
    );
  }

  render() {
    return (
      <div
        className="resize-drag"
        style={{
          height: "320px",
          width: "1080px",
        }}
      >
        <div style={{ height: "100%" }}>
          {this.editorTopBar()}
          <MonacoEditor
            className="no-interactjs-action editor-inner"
            id={this.props.id}
            language={this.props.languageName}
            theme="vs-dark"
            value={this.props.state.code}
            options={this.options}
            onChange={this.onChange}
            editorDidMount={this.editorDidMount}
            dragAndDrop={true}
          />
        </div>
      </div>
    );
  }
}

Editor.propTypes = {
  id: PropTypes.number.isRequired,
  languageName: PropTypes.string.isRequired,
  rulename: PropTypes.string,
};

export default Editor;
