import React from "react";

export const Checkbox = ({ label, value, onChange }) => {
  return (
    <div style={{ marginLeft: "8px" }}>
      <label style={{ width: "100%" }}>
        <input type="checkbox" checked={value} onChange={onChange} />
        {" " + label}
      </label>
    </div>
  );
};

export default Checkbox;
