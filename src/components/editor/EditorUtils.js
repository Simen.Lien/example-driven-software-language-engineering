import * as monaco from "monaco-editor";

/**
 *  Set a new language for the Monaco Editor.
 *  Keywords and identifiers become part of the new language.
 *
 * @param {string} languageName
 * @param {string[]} keywords
 * @param {string[]} identifiers
 * @returns
 */
export function setEditorLanguage(languageName, keywords, identifiers) {
  if (!languageName.includes("test")) {
    // Only set a new editor language if the languageName includes "test".
    // In order to not overwrite languages such as "java", "python" or any other already defined language.
    console.log("Not setting a new editor language. Name:", languageName);
    return;
  }
  let extensionName = "." + languageName;
  monaco.languages.register({
    id: languageName,
    extensions: [extensionName],
  });

  monaco.languages.setLanguageConfiguration(languageName, {
    wordPattern:
      /(-?\d*\.\d\w*)|([\:])|([^\`\~\!\@\#\%\^\&\*\(\)\-\=\+\[\{\]\}\\\|\;\'\"\,\.\<\>\/\?\s]+)/g,

    comments: {
      lineComment: "//",
      blockComment: ["/*", "*/"],
    },
    brackets: [
      ["{", "}"],
      ["[", "]"],
    ],
    autoClosingPairs: [
      {
        open: "{",
        close: "}",
      },
      {
        open: "[",
        close: "]",
      },
    ],
  });
  monacoLanguageSetMonarchTokens(languageName, keywords, identifiers);
}

function monacoLanguageSetMonarchTokens(languageName, keywords, identifiers) {
  monaco.languages.setMonarchTokensProvider(languageName, {
    keywords: keywords,

    typeKeywords: [], //['boolean', 'double', 'byte', 'int', 'short', 'char', 'void', 'long', 'float'],

    operators: [],
    /*[
                  '=', '>', '<', '!', '~', '?', ':', '==', '<=', '>=', '!=',
                  '&&', '||', '++', '--', '+', '-', '*', '/', '&', '|', '^', '%',
                  '<<', '>>', '>>>', '+=', '-=', '*=', '/=', '&=', '|=', '^=',
                  '%=', '<<=', '>>=', '>>>='
                  ],*/

    identifiers: identifiers, // added

    symbols: /[=><!~?:&|+\-*\/\^%]+/,
    escapes:
      /\\(?:[btnfru\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,

    tokenizer: {
      root: [
        // Identifiers and keywords
        [
          /[a-zA-Z_$][\w$:]*/, //was /[a-z_$][\w$]*/
          {
            cases: {
              "@typeKeywords": "keyword",
              "@keywords": "keyword",
              "@identifiers": "type.identifier", // added
              "@default": "identifier",
            },
          },
        ],

        // Whitespace
        { include: "@whitespace" },

        // Delimiters and operators
        [/[{}()\[\]]/, "@brackets"],
        [/[<>](?!@symbols)/, "@brackets"],
        [
          /@symbols/,
          {
            cases: {
              "@operators": "operator",
              "@default": "",
            },
          },
        ],

        [
          /@\s*[a-zA-Z_\$][\w\$]*/,
          { token: "annotation", log: "annotation token: $0" },
        ],
        // numbers
        [/\d*\.\d+([eE][\-+]?\d+)?/, "number.float"],
        [/0[xX][0-9a-fA-F]+/, "number.hex"],
        [/\d+/, "number"],

        // delimiter: after number because of .\d floats
        [/[;,.]/, "delimiter"],

        // strings
        [/"([^"\\]|\\.)*$/, "string.invalid"], // non-teminated string
        [/"/, { token: "string.quote", bracket: "@open", next: "@string" }],

        // characters
        [/'[^\\']'/, "string"],
        [/(')(@escapes)(')/, ["string", "string.escape", "string"]],
        [/'/, "string.invalid"],
      ],

      whitespace: [
        [/[ \t\r\n]+/, "white"],
        [/\/\*/, "comment", "@comment"],
        [/\/\/.*$/, "comment"],
      ],

      comment: [
        [/[^\/*]+/, "comment"],
        [/\/\*/, "comment.invalid"],
        ["\\*/", "comment", "@pop"],
        [/[\/*]/, "comment"],
      ],

      string: [
        [/[^\\"]+/, "string"],
        [/@escapes/, "string.escape"],
        [/\\./, "string.escape.invalid"],
        [/"/, "string", "@pop"],
      ],
    },
  });
}

export function addNewEditorRightClickAction(editor, label, onClick) {
  editor.addAction({
    // An unique identifier of the contributed action.
    id: "unique-id-" + label,

    // A label of the action that will be presented to the user.
    label: label,

    // A precondition for this action.
    precondition: null,

    // A rule to evaluate on top of the precondition in order to dispatch the keybindings.
    keybindingContext: null,

    contextMenuGroupId: "modification",

    contextMenuOrder: 1.5,

    // Method that will be executed when the action is triggered.
    // @param editor The editor instance is passed in as a convinience
    run: function (ed) {
      const selection = ed.getSelection();
      const model = ed.getModel();
      const position = {
        column: selection.positionColumn,
        lineNumber: selection.positionLineNumber,
      };
      const selectedWord = model.getWordAtPosition(position).word;
      onClick(selectedWord);
      return null;
    },
  });
}
