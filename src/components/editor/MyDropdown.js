import React from "react";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Checkbox from "./Checkbox";

const MyDropdown = ({ editorID, title, data, isCheckbox = false }) => {
  return (
    <div className="dropdown">
      <DropdownButton
        id="dropdown-item-button"
        title={title}
        size="sm"
        style={{ paddingLeft: "8px"}}
      >
        {isCheckbox ?
        data.map(({ label, value, onClick }, index) => (
          <div key={index}>
            <Checkbox
              label={label}
              value={value}
              onChange={() => onClick(editorID)}
            />
          </div>
        ))
        :
        data.map(({ label, onClick }, index) => (
          <Dropdown.Item key={index} onClick={onClick} style={{height: "28px"}}>
            {label}
          </Dropdown.Item>
        ))
        }
      </DropdownButton>
    </div>
  );
};

export default MyDropdown;
