import { IModelDecorationOptions } from "react-monaco-editor";

/**
 * Add a new decoration at the given range.
 *
 * @param {*} editor monaco editor to add decorations to
 * @param {string[]} prevDecorations previous decorations
 * @param {IRange} range specifying where to put the new decoration
 * @param {IModelDecorationOptions} options specifying how the decoration should look
 * @returns
 */
function addDecorationAtRange(editor, prevDecorations = [], range, options) {
  if (!editor) {
    return;
  }
  let newDecoration = editor.deltaDecorations(
    [],
    [
      {
        range: range,
        options: options,
      },
    ]
  );
  let decorations = [...prevDecorations, newDecoration];
  return decorations;
}

export function removeAllDecorations(editor, decorations = []) {
  if (!editor) {
    return;
  }
  decorations = editor.deltaDecorations(decorations, []);
  return decorations;
}

/**
 * Add decoration to words that are both in @param wordList and in the editor.
 *
 * For example:
 * Editor has words: "add", "ingredient", "all", "to", "bowl".
 *
 * If @param wordList has words: "add", "all",
 * then a decoration is added to "add" and "all" because it's both in @param wordList and in the editor.
 *
 * The decoration for each word depends on @param options.
 *
 * @param {string[]} wordList a list of words, e.g. all keywords, all identifiers, etc.
 * @param {IModelDecorationOptions} options for a model decoration
 */
export function addDecorations(
  editor,
  wordList = [],
  options = {},
  decorations = []
) {
  if (!editor) {
    return;
  }
  let model = editor.getModel();
  const searchOnlyEditableRange = true;
  const isRegex = false;
  const matchCase = false;
  const wordSeperators = " "; // Only match entire words

  for (const word of wordList) {
    const matches = model.findMatches(
      word,
      searchOnlyEditableRange,
      isRegex,
      matchCase,
      wordSeperators
    );
    for (const match of matches) {
      decorations = addDecorationAtRange(
        editor,
        decorations,
        match.range,
        options
      );
    }
  }
  return decorations;
}
