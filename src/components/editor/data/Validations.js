export const VALIDATIONS = {
  START_WITH_LOWER_CASE: {name: 'StartWithLowerCase'},
  START_WITH_UPPER_CASE: {name: 'StartWithUpperCase'},
  IS_LOWER_CASE: {name: "IsLowerCase"},
  IS_UPPER_CASE: {name: "IsUpperCase"},
  VALUE_BETWEEN_MIN_MAX: {name: 'ValueBetweenMinMax', fields: {min: 0, max: 100}, validationForNumber: true},
  VALUE_NOT_BETWEEN_MIN_MAX: {name: "ValueNotBetweenMinMax", fields: {min: 0, max: 100}, validationForNumber: true},
  VALUE_LESS_THAN: {name: "ValueLessThan", fields: {max: 100}, validationForNumber: true},
  VALUE_LESS_THAN_OR_EQUAL_TO: {name: "ValueLessThanOrEqualTo", fields: {max: 100}, validationForNumber: true},
  VALUE_GREATER_THAN: {name: "ValueGreaterThan", fields: {min: 0}, validationForNumber: true},
  VALUE_GREATER_THAN_OR_EQUAL_TO: {name: "ValueGreaterThanOrEqualTo", fields: {min: 0}, validationForNumber: true},
  EQUAL_TO: {name: "EqualTo", fields: {value: ""}},
  NOT_EQUAL_TO: {name: "NotEqualTo", fields: {value: ""}},
  LENGTH_EQUAL_TO: {name: "LengthEqualTo", fields: {number: 10}},
  LENGTH_NOT_EQUAL_TO: {name: "LengthNotEqualTo", fields: {number: 10}},
  LENGTH_LESS_THAN: {name: "LengthLessThan", fields: {number: 10}},
  LENGTH_GREATER_THAN: {name: "LengthGreaterThan", fields: {number: 10}},
  LENGTH_LESS_THAN_OR_EQUAL_TO: {name: "LengthLessThanOrEqualTo", fields: {number: 10}},
  LENGTH_GREATER_THAN_OR_EQUAL_TO: {name: "LengthGreaterThanOrEqualTo", fields: {number: 10}},
  START_WITH: {name: "StartWith", fields: {value: ""}},
  END_WITH: {name: "EndWith", fields: {value: ""}},
  INCLUDES: {name: "Includes", fields: {value: ""}},
  NOT_INCLUDES: {name: "NotIncludes", fields: {value: ""}},
  //VALUE_FROM_API: {name: "ValueFromAPI", fields: {url: "", value: ""}},
}