import React from "react";

export function createValidationXML(
  allEditorsInfo,
  rules,
  duplicateRulenames = true
) {
  const ReactDomServer = require("react-dom/server");

  const Validations = (props) => React.createElement("validations", props);
  const Validation = (props) => React.createElement("validation", props);

  // Keep track of unique rulenames for when @duplicateRulenames is false.
  let uniqueRulenames = [];

  let elements = [];

  for (const rule of rules) {
    const rulename = Object.values(allEditorsInfo).find(
      (editor) => editor.id === rule.id
    ).rulename;

    // Use to find out if there are multiple validations in one editor
    let numValidations = 0;
    for (const token of rule.tokens) {
      numValidations += token.validations.length;
    }

    for (const token of rule.tokens) {
      if (token.validations.length > 0) {
        for (const validation of token.validations) {
          const node = rulename.toLowerCase();
          const property = token.variableName || token.word;

          const propertyUpperCaseFirstLetter = property.charAt(0).toUpperCase() + property.slice(1);

          const functionName = "check" + rulename + propertyUpperCaseFirstLetter + validation.name;
          const fields = validation?.fields;

          let allProps = {
            name: validation.name,
            rulename: rulename,
            node: node,
            property: property,
            functionname: functionName,
            min: fields?.min,
            max: fields?.max,
            number: fields?.number,
            value: fields?.value,
          };

          if (token.isNumber) {
            allProps = { ...allProps, isnumber: "true" };
          }

          if (numValidations > 1) {
            // More than 1 validation is defined in an editor
            allProps = { ...allProps, multiplevalidations: "true" };
          }

          if (!duplicateRulenames && uniqueRulenames.includes(rulename)) {
            // rulename is already in uniqueRulenames
            continue;
          }
          uniqueRulenames.push(rulename);
          elements = [...elements, <Validation {...allProps} />];
        }
      }
    }
  }
  const elementXML = ReactDomServer.renderToString(
    <Validations>{elements}</Validations>
  );
  return elementXML;
}

export function validationXSLTImportSection(xmlContent) {
  const processor = new XSLTProcessor();
  const domParser = new DOMParser();
  const xmlDoc = domParser.parseFromString(xmlContent, "text/xml");

  const xslt_step_1 = `<?xml version="1.0" encoding="UTF-8"?>
  <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                  xmlns:xs="http://www.w3.org/2001/XMLSchema"
                  exclude-result-prefixes="xs"
                  version="2.0">
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="validations">  
      <xsl:choose>
        <xsl:when test="count(./*) = 0">
          import { ValidationCheck, ValidationRegistry } from 'langium';
          import { LangiumNameTestAstType } from './generated/ast';
          import { LangiumNameTestServices } from './langium-name-test-module';
          /**
          * Map AST node types to validation checks.
          */
          type LangiumNameTestChecks = { [type in LangiumNameTestAstType]?: ValidationCheck | ValidationCheck[] }
        </xsl:when>
        <xsl:when test="count(./*) > 0">
          import { ValidationAcceptor, ValidationCheck, ValidationRegistry } from 'langium'; 
          <br/>
          import {  
          <xsl:apply-templates/> LangiumNameTestAstType } from './generated/ast'; 
          <br/>
          import { LangiumNameTestServices } from './langium-name-test-module'; 
          <br/>
          <br/>
          /** 
          <br/>
          * Map AST node types to validation checks. 
          <br/>
          */ 
          <br/>
          type LangiumNameTestChecks = { [type in LangiumNameTestAstType]?: ValidationCheck | ValidationCheck[] } 
          <br/> 
          <br/>
        </xsl:when>
      </xsl:choose>
    </xsl:template>
    <xsl:template match="//validation">
      <xsl:value-of select="./@rulename"/>,
      <xsl:apply-templates/>
    </xsl:template>
  </xsl:stylesheet>
`;

  const xslStylesheet = domParser.parseFromString(xslt_step_1, "text/xml");
  processor.importStylesheet(xslStylesheet);
  const fragment = processor.transformToFragment(xmlDoc, document);
  return fragment.textContent;
}

export function validationXSLTValidationRegistrySection(xmlContent) {
  const processor = new XSLTProcessor();
  const domParser = new DOMParser();
  const xmlDoc = domParser.parseFromString(xmlContent, "text/xml");
  const xslt_step_1 = `<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:key name="validation-by-filename" match="validation[@filename]" use="@filename" />
    
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="validations">
        <xsl:choose>
            <xsl:when test="count(./*) = 0">
                /**
                * Registry for validation checks.
                */
                export class LangiumNameTestValidationRegistry extends ValidationRegistry {
                constructor(services: LangiumNameTestServices) {
                super(services);
                const validator = services.validation.LangiumNameTestValidator;
                const checks: LangiumNameTestChecks = {
                };
                this.register(checks, validator);
                }
                }
            </xsl:when>
            <xsl:when test="count(./*) > 0">
                export class LangiumNameTestValidationRegistry extends ValidationRegistry { 
                constructor(services: LangiumNameTestServices) { 
                super(services); 
                const validator = services.validation.LangiumNameTestValidator; 
                const checks: LangiumNameTestChecks = { 
                
                
                
                <xsl:for-each select="//validation[count(. | key('validation-by-filename', @filename)[1]) = 1]">
                    <xsl:if test="@multipleValidations">
                        <xsl:value-of select="./@filename"/>: [
                        <xsl:for-each select="key('validation-by-filename', @filename)">
                            validator.<xsl:value-of select="./@rulename"/>    
                            <xsl:if test="position() != last()"><xsl:text>, </xsl:text></xsl:if>
                        </xsl:for-each>                
                        <xsl:text>&#10;</xsl:text>
                        ]
                    </xsl:if>
                    
                    <xsl:if test="not(@multipleValidations)">
                        <xsl:value-of select="./@filename"/>: validator.<xsl:value-of select="./@rulename"/>    
                    </xsl:if>
                    ,
                </xsl:for-each>
                
                
                
                }; 
                this.register(checks, validator); 
                } 
                } 
            </xsl:when>
        </xsl:choose>
    </xsl:template>
        
</xsl:stylesheet>
`;

  const xslStylesheet = domParser.parseFromString(xslt_step_1, "text/xml");
  processor.importStylesheet(xslStylesheet);
  const fragment = processor.transformToFragment(xmlDoc, document);
  return fragment.textContent;
}

export function validationXSLTValidatorSection(xmlContent) {
  const processor = new XSLTProcessor();
  const domParser = new DOMParser();
  const xmlDoc = domParser.parseFromString(xmlContent, "text/xml");
  const xslt_step_1 = `<?xml version="1.0" encoding="UTF-8"?>
  <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                  xmlns:xs="http://www.w3.org/2001/XMLSchema"
                  exclude-result-prefixes="xs"
                  version="2.0">
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="validations">
      <xsl:choose>
        <xsl:when test="count(./*) = 0">
          export class LangiumNameTestValidator {

          }
        </xsl:when>
        <xsl:when test="count(./*) > 0">
              export class LangiumNameTestValidator { <br/>
                  <xsl:apply-templates/>
          } <br/>
        </xsl:when>
      </xsl:choose>
    </xsl:template>
    <xsl:template match="//validation[@name='StartWithLowerCase']">
          <xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void { <br/>
                  if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){ <br/>
                      const firstChar = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>.substring(0, 1); <br/>
                      if(firstChar.toLowerCase() !== firstChar){ <br/>
                          accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should start with a lower case letter.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' }); <br/>
                      } <br/>
                  } <br/>
              } <br/>
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="//validation[@name='StartWithUpperCase']">
          <xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void { <br/>
                  if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){ <br/>
                      const firstChar = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>.substring(0, 1); <br/>
                      if(firstChar.toUpperCase() !== firstChar){ <br/>
                          accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should start with an upper case letter.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' }); <br/>
                      } <br/>
                  } <br/>
              } <br/>
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="//validation[@name='IsLowerCase']">
    <xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void { <br/>
            if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){ <br/>
                const word = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>; <br/>
                if(word.toLowerCase() !== word){ <br/>
                    accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should be lower case.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' }); <br/>
                } <br/>
            } <br/>
        } <br/>
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="//validation[@name='IsUpperCase']">
    <xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void { <br/>
            if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){ <br/>
                const word = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>; <br/>
                if(word.toUpperCase() !== word){ <br/>
                    accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should be upper case.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' }); <br/>
                } <br/>
            } <br/>
        } <br/>
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="//validation[@name='ValueBetweenMinMax']">
      <xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
          if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
              const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
              if(<xsl:value-of select="./@property"/> &lt; <xsl:value-of select="./@min"/> || <xsl:value-of select="./@property"/> &gt; <xsl:value-of select="./@max"/>){
                  accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should be between <xsl:value-of select="./@min"/> and <xsl:value-of select="./@max"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
              }
          }
      }
    </xsl:template>

  <xsl:template match="//validation[@name='ValueNotBetweenMinMax']">
  <xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
        if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
            const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
            if(!(<xsl:value-of select="./@property"/> &lt; <xsl:value-of select="./@min"/> || <xsl:value-of select="./@property"/> &gt; <xsl:value-of select="./@max"/>)){
                accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should not be between <xsl:value-of select="./@min"/> and <xsl:value-of select="./@max"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
            }
        }
    }
  </xsl:template>

  <xsl:template match="//validation[@name='ValueLessThan']">
  <xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
        if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
            const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
            if(<xsl:value-of select="./@property"/> >= <xsl:value-of select="./@max"/>){
                accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should be less than <xsl:value-of select="./@max"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
            }
        }
    }
  </xsl:template>

  <xsl:template match="//validation[@name='ValueLessThanOrEqualTo']">
  <xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
      if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
          const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
          if(<xsl:value-of select="./@property"/> > <xsl:value-of select="./@max"/>){
              accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should be less than or equal to <xsl:value-of select="./@max"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
          }
      }
  }
  </xsl:template>

    <xsl:template match="//validation[@name='ValueGreaterThan']">
    <xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
        if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
            const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
            if(!(<xsl:value-of select="./@property"/> > <xsl:value-of select="./@min"/>)){
                accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should be greater than <xsl:value-of select="./@min"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
            }
        }
    }
  </xsl:template>

  <xsl:template match="//validation[@name='ValueGreaterThanOrEqualTo']">
  <xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
      if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
          const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
          if(!(<xsl:value-of select="./@property"/> >= <xsl:value-of select="./@min"/>)){
              accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should be greater than or equal to <xsl:value-of select="./@min"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
          }
      }
  }
</xsl:template>

<xsl:template match="//validation[@name='EqualTo']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
  if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
  <xsl:choose>
    <xsl:when test="@isnumber='true'">
      const <xsl:value-of select="./@property"/>: number = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
      if(<xsl:value-of select="./@property"/> !== <xsl:value-of select="./@value"/>){
        accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should be equal to <xsl:value-of select="./@value"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
      }
    </xsl:when>
    <xsl:otherwise>
      const <xsl:value-of select="./@property"/>: string = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(<xsl:value-of select="./@property"/> !== '<xsl:value-of select="./@value"/>'){
          accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should be equal to "<xsl:value-of select="./@value"/>".', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
      }
    </xsl:otherwise>
  </xsl:choose>
    }
}
</xsl:template>

<xsl:template match="//validation[@name='NotEqualTo']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
  if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
  <xsl:choose>
    <xsl:when test="@isnumber='true'">
      const <xsl:value-of select="./@property"/>: number = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
      if(<xsl:value-of select="./@property"/> === <xsl:value-of select="./@value"/>){
        accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should not be equal to <xsl:value-of select="./@value"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
      }
    </xsl:when>
    <xsl:otherwise>
      const <xsl:value-of select="./@property"/>: string = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(<xsl:value-of select="./@property"/> === '<xsl:value-of select="./@value"/>'){
          accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should not be equal to "<xsl:value-of select="./@value"/>".', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
      }
    </xsl:otherwise>
  </xsl:choose>
    }
}
</xsl:template>

<xsl:template match="//validation[@name='LengthEqualTo']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
    if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
        const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(<xsl:value-of select="./@property"/>.length !== <xsl:value-of select="./@number"/>){
            accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> length should be equal to <xsl:value-of select="./@number"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
        }
    }
}
</xsl:template>

<xsl:template match="//validation[@name='LengthNotEqualTo']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
    if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
        const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(<xsl:value-of select="./@property"/>.length === <xsl:value-of select="./@number"/>){
            accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> length should not be equal to <xsl:value-of select="./@number"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
        }
    }
}
</xsl:template>

<xsl:template match="//validation[@name='LengthLessThan']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
    if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
        const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(<xsl:value-of select="./@property"/>.length >= <xsl:value-of select="./@number"/>){
            accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> length should be less than <xsl:value-of select="./@number"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
        }
    }
}
</xsl:template>

<xsl:template match="//validation[@name='LengthGreaterThan']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
    if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
        const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(!(<xsl:value-of select="./@property"/>.length > <xsl:value-of select="./@number"/>)){
            accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> length should be greater than <xsl:value-of select="./@number"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
        }
    }
}
</xsl:template>

<xsl:template match="//validation[@name='LengthLessThanOrEqualTo']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
    if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
        const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(<xsl:value-of select="./@property"/>.length > <xsl:value-of select="./@number"/>){
            accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> length should be less than equal to <xsl:value-of select="./@number"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
        }
    }
}
</xsl:template>

<xsl:template match="//validation[@name='LengthGreaterThanOrEqualTo']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
    if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
        const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(!(<xsl:value-of select="./@property"/>.length >= <xsl:value-of select="./@number"/>)){
            accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> length should be greater than or equal to <xsl:value-of select="./@number"/>.', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
        }
    }
}
</xsl:template>

<xsl:template match="//validation[@name='StartWith']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
    if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
        const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(!(<xsl:value-of select="./@property"/>.startsWith('<xsl:value-of select="./@value"/>'))){
            accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should start with "<xsl:value-of select="./@value"/>".', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
        }
    }
}
</xsl:template>

<xsl:template match="//validation[@name='EndWith']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
    if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
        const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(!(<xsl:value-of select="./@property"/>.endsWith('<xsl:value-of select="./@value"/>'))){
            accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should end with "<xsl:value-of select="./@value"/>".', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
        }
    }
}
</xsl:template>

<xsl:template match="//validation[@name='Includes']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
    if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
        const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(!(<xsl:value-of select="./@property"/>.includes('<xsl:value-of select="./@value"/>'))){
            accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should include "<xsl:value-of select="./@value"/>".', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
        }
    }
}
</xsl:template>

<xsl:template match="//validation[@name='NotIncludes']">
<xsl:value-of select="./@functionname"/>(<xsl:value-of select="./@node"/>: <xsl:value-of select="./@rulename"/>, accept: ValidationAcceptor): void {
    if(<xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>){
        const <xsl:value-of select="./@property"/> = <xsl:value-of select="./@node"/>.<xsl:value-of select="./@property"/>;
        if(<xsl:value-of select="./@property"/>.includes('<xsl:value-of select="./@value"/>')){
            accept('warning', '<xsl:value-of select="./@rulename"/>&#160;<xsl:value-of select="./@property"/> should not include "<xsl:value-of select="./@value"/>".', { node: <xsl:value-of select="./@node"/>, property: '<xsl:value-of select="./@property"/>' });
        }
    }
}
</xsl:template>

  </xsl:stylesheet>
`;
  const xslStylesheet = domParser.parseFromString(xslt_step_1, "text/xml");
  processor.importStylesheet(xslStylesheet);
  const fragment = processor.transformToFragment(xmlDoc, document);
  return fragment.textContent;
}




export function rulesXSLTransformation(xmlContent, predefinedGrammar) {
  const processor = new XSLTProcessor();
  const domParser = new DOMParser();
  const xmlDoc = domParser.parseFromString(xmlContent, "text/xml");
  const xslt_step_1 = `<?xml version="1.0" encoding="UTF-8"?>
  <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:xs="http://www.w3.org/2001/XMLSchema"
      exclude-result-prefixes="xs"
      version="2.0">
      
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="rules">
    grammar LangiumNameTest <br/>

    hidden(WS, ML_COMMENT, SL_COMMENT)
    terminal WS: /\\s+/; <br/>
    terminal ML_COMMENT: /\\/\\*[\\s\\S]*?\\*\\//; <br/>
    terminal SL_COMMENT: /\\/\\/[^\\n\\r]*/; <br/>
    terminal STRING: /"[^"]*"|'[^']*'/; <br/>
    terminal ID: /[_a-zA-Z][\\w_]*/; <br/>
    terminal INT returns number: /-?[0-9]+/; <br/>
  

      ${predefinedGrammar}

      <xsl:apply-templates/>
  </xsl:template>
      
      <xsl:template match="enum">
          enum <xsl:value-of select="./@id"/>: <xsl:apply-templates/>; <br/>
      </xsl:template>
  
      <xsl:template match="enum/element[not(position()=last())]">
          <xsl:value-of select="./@value"/>='<xsl:value-of select="./@value"/>' |
      </xsl:template>
      
      <xsl:template match="enum/element[position()=last()]">
          <xsl:value-of select="./@value"/>='<xsl:value-of select="./@value"/>'
      </xsl:template>
  
      <xsl:template match="rule">
          <xsl:value-of select="./@id"/>: <xsl:apply-templates/>;
      </xsl:template>
  
      <xsl:template match="rule/keyword">
          '<xsl:value-of select="./@id"/>'
      </xsl:template>

      <xsl:template match="rule/string[not(@variablename)]">
          <xsl:value-of select="./@id"/>=STRING
      </xsl:template>

      <xsl:template match="rule/string[@variablename]">
          <xsl:value-of select="./@variablename"/>=STRING
      </xsl:template>

      <xsl:template match="//rule/token/alternatives">
          (<xsl:apply-templates/>)
      </xsl:template>
  
      <xsl:template match="//rule/token/alternatives/element[not(position()=last())]">
          '<xsl:value-of select="./@value"/>' |
      </xsl:template>
  
          <xsl:template match="//rule/token/alternatives/element[position()=last()]">
          '<xsl:value-of select="./@value"/>'
      </xsl:template>
      
      <xsl:template match="rule/token[@variablename and not(@crossreference) and not(@rulereference)]">
          <xsl:choose>
              <xsl:when test="not(*) and @identifier">
                  <xsl:value-of select="./@variablename"/>=ID
                  <xsl:apply-templates/>
              </xsl:when>
              <xsl:otherwise>
                  <xsl:value-of select="./@variablename"/>=
                  <xsl:apply-templates/>
              </xsl:otherwise>
          </xsl:choose>
      </xsl:template>
  
      <xsl:template match="rule/token[@id and not(@variablename) and not(@crossreference) and not(@rulereference)]">
          <xsl:choose>
              <xsl:when test="not(*) and @identifier">
                  <xsl:value-of select="./@id"/>=ID
                  <xsl:apply-templates/>
              </xsl:when>
              <xsl:otherwise>
              <xsl:value-of select="./@id"/>=
              <xsl:apply-templates/>
              </xsl:otherwise>
          </xsl:choose>
      </xsl:template>
  
      <xsl:template match="rule/token[@crossreference and @variablename]">
          <xsl:value-of select="./@variablename"/>=[<xsl:value-of select="./@crossreference"/>]
          <xsl:if test="@cardinality">
            <xsl:value-of select="./@cardinality"/>
          </xsl:if>
          &#160;
      </xsl:template>
  
      <xsl:template match="rule/token[@id and @crossreference and not(@variablename)]">
          <xsl:value-of select="./@id"/>=[<xsl:value-of select="./@crossreference"/>]
          <xsl:if test="@cardinality">
            <xsl:value-of select="./@cardinality"/>
          </xsl:if>
          &#160;
      </xsl:template>
  
      <xsl:template match="rule/token[@rulereference and @variablename and not(@cardinality)]">
          <xsl:value-of select="./@variablename"/>=<xsl:value-of select="./@rulereference"/> &#160;
      </xsl:template>

      <xsl:template match="rule/token[@rulereference and @variablename and @cardinality]">
      <xsl:choose>
        <xsl:when test="@cardinality='*' or @cardinality='+'">
          <xsl:value-of select="./@variablename"/>+=
          <xsl:value-of select="./@rulereference"/>
          <xsl:value-of select="./@cardinality"/>&#160;
        </xsl:when>
        <xsl:when test="@cardinality='?'">
          <xsl:value-of select="./@variablename"/>?=
          <xsl:value-of select="./@rulereference"/>
          <xsl:value-of select="./@cardinality"/>&#160;
        </xsl:when>
      </xsl:choose>
    </xsl:template>
    <xsl:template match="rule/token[@id and @rulereference and not(@variablename) and not(@cardinality)]">
      <xsl:value-of select="./@id"/>=
      <xsl:value-of select="./@rulereference"/> &#160;
    </xsl:template>
    <xsl:template match="rule/token[@id and @rulereference and not(@variablename) and @cardinality]">
      <xsl:choose>
        <xsl:when test="@cardinality='*' or @cardinality='+'">
          <xsl:value-of select="./@id"/>+=
          <xsl:value-of select="./@rulereference"/>
          <xsl:value-of select="./@cardinality"/>&#160;
        </xsl:when>
        <xsl:when test="@cardinality='?'">
          <xsl:value-of select="./@id"/>?=
          <xsl:value-of select="./@rulereference"/>
          <xsl:value-of select="./@cardinality"/>&#160;
        </xsl:when>
      </xsl:choose>
    </xsl:template>

      <xsl:template match="rule/integer[@id and not(@variablename)]">
          amount=INT
      </xsl:template>
      
      <xsl:template match="rule/integer[@variablename]">
          <xsl:value-of select="./@variablename"/>=INT
      </xsl:template>
      
  </xsl:stylesheet>
`;
  const xslStylesheet = domParser.parseFromString(xslt_step_1, "text/xml");
  processor.importStylesheet(xslStylesheet);
  const fragment = processor.transformToFragment(xmlDoc, document);
  return fragment.textContent;
}


export function generatorTsXSLTransformation(xmlContent) {
  const processor = new XSLTProcessor();
  const domParser = new DOMParser();
  const xmlDoc = domParser.parseFromString(xmlContent, "text/xml");
  const xslt_step_1 = `<?xml version="1.0" encoding="UTF-8"?>
  <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                  xmlns:xs="http://www.w3.org/2001/XMLSchema"
                  exclude-result-prefixes="xs"
                  version="2.0">
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="//rule">
      import fs from 'fs';
      import { CompositeGeneratorNode, NL, processGeneratorNode } from 'langium';
      import { 
      <xsl:value-of select="./@id"/> } from '../language-server/generated/ast';
      import { extractDestinationAndName } from './cli-util';
      import path from 'path';
      export function generateJavaScript(
      <xsl:value-of select="./@idlowercase"/>: 
      <xsl:value-of select="./@id"/>, filePath: string, destination: string | undefined): string {
      const data = extractDestinationAndName(filePath, destination);
      const generatedFilePath = \`\${path.join(data.destination, data.name)}.js\`;
      const fileNode = new CompositeGeneratorNode();
      fileNode.append('"use strict";', NL, NL);
      //model.greetings.forEach(greeting => fileNode.append(\`console.log('Hello, \${greeting.person.$refText}!');\`, NL));
      if (!fs.existsSync(data.destination)) {
      fs.mkdirSync(data.destination, { recursive: true });
      }
      fs.writeFileSync(generatedFilePath, processGeneratorNode(fileNode));
      return generatedFilePath;
      }
    </xsl:template>
  </xsl:stylesheet>
`;
  const xslStylesheet = domParser.parseFromString(xslt_step_1, "text/xml");
  processor.importStylesheet(xslStylesheet);
  const fragment = processor.transformToFragment(xmlDoc, document);
  return fragment.textContent;
}


export function indexTsXSLTransformation(xmlContent) {
  const processor = new XSLTProcessor();
  const domParser = new DOMParser();
  const xmlDoc = domParser.parseFromString(xmlContent, "text/xml");
  const xslt_step_1 = `<?xml version="1.0" encoding="UTF-8"?>
  <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                  xmlns:xs="http://www.w3.org/2001/XMLSchema"
                  exclude-result-prefixes="xs"
                  version="2.0">
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="//rule">
        import colors from 'colors';
        import { Command } from 'commander';
        import { languageMetaData } from '../language-server/generated/module';
        import { <xsl:value-of select="./@id"/> } from '../language-server/generated/ast';
        import { createLangiumNameTestServices } from '../language-server/langium-name-test-module';
        import { extractAstNode } from './cli-util';
        import { generateJavaScript } from './generator';

        export const generateAction = async (fileName: string, opts: GenerateOptions): Promise&lt;void> => {
            const <xsl:value-of select="./@idlowercase"/> = await extractAstNode&lt;<xsl:value-of select="./@id"/>>(fileName, languageMetaData.fileExtensions, createLangiumNameTestServices());
            const generatedFilePath = generateJavaScript(<xsl:value-of select="./@idlowercase"/>, fileName, opts.destination);
            console.log(colors.green(\`JavaScript code generated successfully: \${generatedFilePath}\`));
        };

        export type GenerateOptions = {
            destination?: string;
        }

        export default function(): void {
            const program = new Command();

            program
                // eslint-disable-next-line @typescript-eslint/no-var-requires
                .version(require('../../package.json').version);

            program
                .command('generate')
                .argument('&lt;file>', \`possible file extensions: \${languageMetaData.fileExtensions.join(', ')}\`)
                .option('-d, --destination &lt;dir>', 'destination directory of generating')
                .description('generates JavaScript code that prints "Hello, {name}!" for each greeting in a source file')
                .action(generateAction);

            program.parse(process.argv);
        }

    </xsl:template>
  </xsl:stylesheet>
`;
  const xslStylesheet = domParser.parseFromString(xslt_step_1, "text/xml");
  processor.importStylesheet(xslStylesheet);
  const fragment = processor.transformToFragment(xmlDoc, document);
  return fragment.textContent;
}

export function sortRulesToIsRootFirst(rules) {
  let sortedRules = [];
  let firstElem = Object.values(rules).find((obj) => obj.isRoot === true);
  if (firstElem !== undefined) {
    sortedRules.push(firstElem);
    let remainingRules = Object.values(rules).filter(
      (obj) => obj !== firstElem
    );
    for (const rule of remainingRules) {
      sortedRules.push(rule);
    }
    return sortedRules;
  } else {
    return rules;
  }
}

function findChildren(rules, potentialParentRule) {
  let children = [];
  for (const rule of Object.values(rules)) {
    if (rule.parentID >= 0 && rule?.parentID === potentialParentRule?.id) {
      children.push(rule);
    }
  }
  return children;
}

export function createRulesXML(allEditorsInfo, rules) {
  const ReactDomServer = require("react-dom/server");

  const Rules = (props) => React.createElement("rules", props);
  const Rule = (props) => React.createElement("rule", props);
  const Keyword = (props) => React.createElement("keyword", props);
  const String = (props) => React.createElement("string", props);
  const Token = (props) => React.createElement("token", props);
  const Integer = (props) => React.createElement("integer", props);
  const Elem = (props) => React.createElement("element", props);

  //console.log("Rules:", rules);
  let sortedRules = sortRulesToIsRootFirst(rules);

  let allXMLRules = [];
  for (const rule of sortedRules) {
    const editor = Object.values(allEditorsInfo).find(
      (editor) => editor.id === rule.id
    );
    const code = rule.code;
    let children = findChildren(rules, rule);

    if (children.length > 1) {
      // More than 1 editor has rule as parent.
      // Grammar for these rules are generated directly from JS, so we don't create XML for them.
      // Skip doing anything here.
      continue;
    }

    let elements = [];
    for (const token of Object.values(rule.tokens)) {
      const allProps = getAllProps(token, allEditorsInfo);
      if (token.isKeyword) {
        elements = [...elements, <Keyword {...allProps} />];
      } 
      else if (token.isNumber) {
        elements = [...elements, <Integer {...allProps} />];
      }
      else if(token.isString) {
        elements = [...elements, <String {...allProps} />];
      }
      else{
        let childrenAlternative = [];
        if(token.alternatives !== ""){
          const alternativesElement = createAlternativesElements(token, Elem);
          childrenAlternative = [...childrenAlternative, alternativesElement];
        }
        if (childrenAlternative.length > 0) {
          const tokenElem = React.createElement(
            "token",
            { ...allProps },
            childrenAlternative
          );
          elements = [...elements, tokenElem];
        }else{
          elements = [...elements, <Token {...allProps} />];
        }
      }
    }

    const parentRulename = getEditorRulename(allEditorsInfo, rule.parentID);
    //console.log("Parent rulename:", parentRulename);

    allXMLRules = [
      ...allXMLRules,
      // Note: parent is only added if it's not undefined.
      <Rule
        key={editor.id}
        id={editor.rulename}
        idlowercase={editor.rulename.toLowerCase()}
        code={code}
        isroot={rule.isRoot.toString()}
        parent={parentRulename}
      >
        {elements}
      </Rule>,
    ];
  }
  const elementXML = ReactDomServer.renderToString(
    <Rules>{allXMLRules}</Rules>
  );
  //console.log(elementXML);
  return elementXML;
}

function createAlternativesElements(token, Elem) {
  const allChildren = [];
  for (const alternative of token.alternatives.split("|")) {
    const element = <Elem value={alternative} />;
    allChildren.push(element);
  }
  const alternatives = React.createElement("alternatives", {}, allChildren);
  return alternatives;
}

// Returns undefined if editor with @id is not found (e.g., when @id is -1).
function getEditorRulename(allEditorsInfo, id) {
  return Object.values(allEditorsInfo).find((obj) => obj.id === id)?.rulename;
}

export function createPredefinedGrammar(allEditorsInfo, rules) {
  let predefinedGrammar = "";
  let sortedRules = sortRulesToIsRootFirst(rules);
  for (const rule of sortedRules) {
    let children = findChildren(rules, rule);
    if (children.length > 1) {
      const grammarForRuleWithChildren = createGrammarForRuleWithChildren(
        allEditorsInfo,
        rule,
        children
      );
      predefinedGrammar += grammarForRuleWithChildren;
    }
  }
  return predefinedGrammar;
}

function createGrammarForRuleWithChildren(allEditorsInfo, rule, childrenArr) {
  if (childrenArr.length <= 1) {
    console.error(
      "Trying to create XML rule for children when there aren't multiple children."
    );
    return;
  }
  // Children.length > 1
  // rulename:
  // if cardinality, surround with parantheses
  // insert all children (add += if cardinality), with each followedy by "|"
  // add parantheses, and insert cardinality.

  let str = "";
  /*
  if(rule.isRoot){
    str += "entry ";
  }
  */
  const rulename = getEditorRulename(allEditorsInfo, rule.id);

  // rulename:
  str += rulename + ": \n";

  for (const token of rule.tokens) {
    if (
      token.cardinality === "0..N" ||
      token.cardinality === "1..N" ||
      token.cardinality === "0..1"
    ) {
      // Cardinality
      // Example
      // Model: (steps+=Steps | tools+=Tool | ingredients+=Ingredient)*;

      // Add outer (
      str += "(";
      // Add children
      for (const [i, child] of childrenArr.entries()) {
        const childrenRulename = getEditorRulename(allEditorsInfo, child.id);
        // insert all children, where e.g., child Tool, becomes: tools+=Tool.
        // Each rule except the last one is followed by " | ".
        const lowerCaseChildrenRulename = childrenRulename.toLowerCase();
        const lastChar = lowerCaseChildrenRulename.slice(-1);

        str += lowerCaseChildrenRulename;
        if (lastChar !== "s") {
          str += "s";
        }

        if (token.cardinality === "0..1") {
          str += "?=" + childrenRulename;
        } else {
          str += "+=" + childrenRulename;
        }

        if (i !== childrenArr.length - 1) {
          // Add if not last child
          str += " | ";
        }
      }
      // Add outer )
      str += ")";

      // Add cardinality
      if (token.cardinality === "0..N") {
        str += "*";
      } else if (token.cardinality === "1..N") {
        str += "+";
      } else if (token.cardinality === "0..1") {
        str += "?";
      }
      str += "; \n\n";
    } else {
      // No cardinality
      // Example
      // NewStep: StepAddIngredientAllToTool |
      //          StepAddIngredientAmountToTool |
      //          StepAddFromToolAllToTool |
      //          StepSetTemperatureToolToAmountDegree |
      //          StepWaitToolForAmountTime |
      //          StepMixTool;

      // For each child: Insert rulename as well as "|" (if not last child).
      for (const [i, child] of childrenArr.entries()) {
        const childrenRulename = getEditorRulename(allEditorsInfo, child.id);
        str += childrenRulename;
        if (i !== childrenArr.length - 1) {
          // Add if not last child
          str += " | \n";
        }
      }
      str += "; \n\n";
    }
  }
  return str;
}

function getAllProps(token, allEditorsInfo) {
  let allProps = {};
  if (token.word !== "") {
    allProps = { ...allProps, id: token.word };
  }
  if(token.isIdentifier){
    allProps = {...allProps, identifier: "true"}
  }
  if (token.variableName !== "") {
    allProps = { ...allProps, variablename: token.variableName };
  }
  if (token.crossReference !== "") {
    allProps = {
      ...allProps,
      crossreference: allEditorsInfo[token.crossReference]?.rulename,
    };
  }
  if (token.ruleReference !== "") {
    allProps = {
      ...allProps,
      rulereference: allEditorsInfo[token.ruleReference]?.rulename,
    };
  }
  if (token.cardinality !== "") {
    let cardinality = "";
    if (token.cardinality === "0..N") {
      cardinality = "*";
    } else if (token.cardinality === "1..N") {
      cardinality = "+";
    } else if (token.cardinality === "0..1") {
      cardinality = "?";
    }
    // Do nothing for cardinality 1
    allProps = { ...allProps, cardinality: cardinality };
  }
  return allProps;
}
