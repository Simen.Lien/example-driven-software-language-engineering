import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ValidationDropdown from "./ValidationDropdown";

export function ValidationModal({
  show,
  handleShow,
  handleClose,
  validationData,
  selectedWord,
  addValidationForSelectedWord,
  removeValidation,
  editorState,
}) {
  const [validationText, setValidationText] = useState(validationData[0]?.name);

  const minInput = (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <label className="input-group-text" htmlFor="inputMin">
          Min:
        </label>
      </div>
      <input id="inputMin" type="number" defaultValue="1"></input>
    </div>
  );

  const maxInput = (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <label className="input-group-text" htmlFor="inputMax">
          Max:
        </label>
      </div>
      <input id="inputMax" type="number" defaultValue="10"></input>
    </div>
  );

  const inputNumber = (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <label className="input-group-text" htmlFor="inputNumber">
          Number:
        </label>
      </div>
      <input id="inputNumber" type="number" defaultValue="10"></input>
    </div>
  );

  const inputURL = (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <label className="input-group-text" htmlFor="inputURL">
          URL:
        </label>
      </div>
      <input id="inputURL" type="text" defaultValue=""></input>
    </div>
  );

  const valueInput = (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <label className="input-group-text" htmlFor="inputValue">
          Value:
        </label>
      </div>
      <input id="inputValue" type="text" defaultValue=""></input>
    </div>
  );

  const displayWord = (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <label className="input-group-text" htmlFor="word">
          Word:
        </label>
      </div>
      <input
        id="word"
        type="text"
        disabled={true}
        defaultValue={selectedWord}
      ></input>
    </div>
  );

  const displayNewValidationChoice = (
    <div>
      <div className="input-group mb-3" style={{ paddingTop: "20px" }}>
        <div className="input-group-prepend">
          <label className="input-group-text" htmlFor="validation">
            New validation:
          </label>
        </div>
        <ValidationDropdown
          id="validation"
          title={validationText}
          validationData={validationData}
          setValidationText={setValidationText}
          isWordNumber={isWordNumber(selectedWord)}
        />
      </div>
      {hasMinMax(validationText) ? (
        <div>
          {minInput} {maxInput}
        </div>
      ) : null}
      {hasOnlyMin(validationText) ? <div>{minInput}</div> : null}
      {hasOnlyMax(validationText) ? <div>{maxInput}</div> : null}
      {hasNumber(validationText) ? <div>{inputNumber}</div> : null}
      {hasURL(validationText) ? <div>{inputURL}</div> : null}
      {hasValue(validationText) ? <div>{valueInput}</div> : null}

      <Button variant="success" onClick={submit}>Add validation</Button>
    </div>
  );

  function getToken(word) {
    return editorState?.tokens.find((token) => token?.word === word);
  }

  function getValidation(validationName) {
    return validationData.find(
      (validation) => validation.name === validationName
    );
  }

  function hasAnyValidation(word) {
    return getToken(word)?.validations.length > 0;
  }

  function hasMinMax(validationName) {
    const validation = getValidation(validationName);
    return (
      validation?.fields?.min !== undefined &&
      validation?.fields?.max !== undefined
    );
  }

  function hasOnlyMin(validationName) {
    const validation = getValidation(validationName);
    return (
      validation?.fields?.min !== undefined &&
      validation?.fields?.max === undefined
    );
  }

  function hasOnlyMax(validationName) {
    const validation = getValidation(validationName);
    return (
      validation?.fields?.min === undefined &&
      validation?.fields?.max !== undefined
    );
  }

  function hasNumber(validationName) {
    const validation = getValidation(validationName);
    return validation?.fields?.number !== undefined;
  }

  function hasURL(validationName) {
    const validation = getValidation(validationName);
    return validation?.fields?.url !== undefined;
  }

  function hasValue(validationName) {
    const validation = getValidation(validationName);
    return validation?.fields?.value !== undefined;
  }

  function isWordNumber(word){
    return getToken(word)?.isNumber;
  }

  function submit() {
    const inputMin = document.getElementById("inputMin");
    const inputMax = document.getElementById("inputMax");
    const inputNumber = document.getElementById("inputNumber");
    const inputURL = document.getElementById("inputURL");
    const inputValue = document.getElementById("inputValue");

    const radix = 10;
    const answerFields = {
      // adding if not undefined
      ...(inputMin?.value && { min: parseInt(inputMin?.value, radix) }),
      ...(inputMax?.value && { max: parseInt(inputMax?.value, radix) }),
      ...(inputNumber?.value && {
        number: parseInt(inputNumber?.value, radix),
      }),
      ...(inputURL?.value && { url: inputURL?.value }),
      ...(inputValue?.value && { value: inputValue?.value }),
    };

    addValidationForSelectedWord(validationText, answerFields);
    //handleClose();
    return answerFields;
  }

  const displayExistingValidations = getToken(selectedWord)?.validations.map(
    (validation, index) => (
      <div className="input-group mb" key={index}>
        <div
          className="input-group-prepend"
          style={{ display: "flex", flexDirection: "row", paddingTop: "10px" }}
        >
          {/*<label className="input-group-text" htmlFor="validationsForWord">
            Existing validation
    </label>*/}
          <input
            id="validationsForWord"
            type="text"
            disabled={true}
            defaultValue={`${validation?.name}`}
            style={{width: "300px"}}
          ></input>
          <Button
            variant={"danger"}
            style={{
              marginLeft: "8px",
            }}
            onClick={() => {
              console.log("Selected word:", selectedWord);
              console.log("Validation name:", validation?.name);
              removeValidation(selectedWord, validation?.name);
            }}
          >
            Delete
          </Button>
        </div>
      </div>
    )
  );

  const displayExsVals = (
    <div className="input-group mb-3" style={{ paddingTop: "40px"}}>
      <div className="input-group-prepend" style={{ width: "300px"}}>
        <label className="input-group-text" style={{alignSelf: "center"}}>Existing validations</label>
      </div>
      {displayExistingValidations}
    </div>
  );

  return (
    <>
      <Button
        size="sm"
        style={{ maxWidth: "10%", marginLeft: "8px" }}
        variant="primary"
        onClick={handleShow}
      >
        Validation
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        size="lg"
      >
        <Modal.Header closeButton>
          <Modal.Title>Select validation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            style={{
              padding: "0px",
              fontSize: "large",
              margin: "10px",
            }}
          >
            {displayWord}
            {displayNewValidationChoice}

            {hasAnyValidation(selectedWord) ? displayExsVals : null}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          {/*<Button variant="primary" onClick={submit}>
            Confirm
          </Button>*/}
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ValidationModal;
