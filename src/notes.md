# Software languages
* What they are, why we use them, examples of who uses them
* The different types of software languages
    * GPL
    * DSL
        * Internal
        * External

* Language workbench
    * What it is
    * Why use it
    * Examples of different LWBS (Jetbrains MPS, Spoofax, Eclipse Xtext, Langium)
    
Provide examples of the different types of software languages, and reasons to use them.
A DSL needs IDE support. An easy way to develop an IDE that supports a DSL is to use a Language Workbench. The Language Workbench we use is Langium. It has a syntax similar to Eclipse Xtext.

# What is software language engineering
What it is, examples of how we can develop new languages (LWB)


# Motivation
It is difficult to create new DSLs, as it requires a lot of knowledge from developers. We wish to make it easier to create new DSLs, and IDE to support it.

## Why create DSLs
* Easier to collaborate with non-developer domain experts because the DSL is domain-specific and therefore easier to understand and work with for those who know the domain.
* Can ease development
* Easier to read code, find mistakes, make changes
* Limited expressivness. Makes it harder to write the wrong code and easier to find errors.
* Increased productivity

## Why create DSL in an example-driven way
* Faster to develop
* Easier to develop. 
* Requires less knowledge about programming (lowers the barrier of entry for people to create DSLs)
* Beginner programmers, and even domain-experts might be able to define a DSL by using an example-driven way to create them.

# Overview of our tool
We have developed a tool where the user can the language specification of a DSL by giving examples of how their language should work, and then, using these language specifications we create grammar and validation rules for Langium such that we can create a DSL and IDE support for the DSL.
    
We want to make it easier to create DSLs, and so we've created a tool where the user can do this quickly and without much knowledge about creating DSL.

# Implementation

