import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { ACTIONS } from "./App";

const PropertyCopyModal = ({
  show,
  handleClose,
  modalWordToCopyState,
  dispatch,
}) => {
  function initValidationModalCheckboxState() {
    let validations = {};
    for (const validation of modalWordToCopyState.validations) {
      validations = { ...validations, [validation.name]: false };
    }
    return validations;
  }

  const validationCheckboxState = {
    ...initValidationModalCheckboxState,
  };

  const initialDragDropModalCheckboxState = {
    keyword: false,
    identifier: false,
    number: false,
    string: false,
    alternatives: false,
    variableName: false,
    crossReference: false,
    ruleReference: false,
    cardinality: false,
    ...validationCheckboxState,
  };

  const [modalCheckboxState, setModalCheckboxState] = useState({
    ...initialDragDropModalCheckboxState,
  });

  function resetModalCheckboxState() {
    setModalCheckboxState((prev) => ({
      ...initialDragDropModalCheckboxState,
    }));
  }

  const checkboxData = [
    {
      name: "keyword",
      checked: modalCheckboxState.keyword,
      checkbox: {
        id: "keyword",
        input: {
          //🔑
          " Keyword": (
            <input
              name="Keyword"
              type="checkbox"
              id="checkKeyword"
              onChange={() =>
                setModalCheckboxState((prev) => ({
                  ...prev,
                  keyword: !prev.keyword,
                }))
              }
              checked={modalCheckboxState.keyword}
            ></input>
          ),
        },
      },
      func: () =>
        dispatch({
          type: ACTIONS.ADD_KEYWORD,
          payload: {
            id: modalWordToCopyState.id,
            key: modalWordToCopyState.wordTo,
          },
        }),
    },
    {
      name: "identifier",
      checked: modalCheckboxState.identifier,
      checkbox: {
        id: "identifier",
        input: {
          //🆔
          " Identifier": (
            <input
              name="Identifier"
              type="checkbox"
              id="checkIdentifier"
              onChange={() =>
                setModalCheckboxState((prev) => ({
                  ...prev,
                  identifier: !prev.identifier,
                }))
              }
              checked={modalCheckboxState.identifier}
            ></input>
          ),
        },
      },
      func: () =>
        dispatch({
          type: ACTIONS.ADD_IDENTIFIER,
          payload: {
            id: modalWordToCopyState.id,
            key: modalWordToCopyState.wordTo,
          },
        }),
    },
    {
      name: "number",
      checked: modalCheckboxState.number,
      checkbox: {
        id: "number",
        input: {
          //🔢
          " Number": (
            <input
              name="Number"
              type="checkbox"
              id="checkNumber"
              onChange={() =>
                setModalCheckboxState((prev) => ({
                  ...prev,
                  number: !prev.number,
                }))
              }
              checked={modalCheckboxState.number}
            ></input>
          ),
        },
      },
      func: () =>
        dispatch({
          type: ACTIONS.ADD_NUMBER,
          payload: {
            id: modalWordToCopyState.id,
            key: modalWordToCopyState.wordTo,
          },
        }),
    },
    {
      name: "string",
      checked: modalCheckboxState.string,
      checkbox: {
        id: "string",
        input: {
          //🔤
          " String": (
            <input
              name="string"
              type="checkbox"
              id="checkString"
              onChange={() =>
                setModalCheckboxState((prev) => ({
                  ...prev,
                  string: !prev.string,
                }))
              }
              checked={modalCheckboxState.string}
            ></input>
          ),
        },
      },
      func: () =>
        dispatch({
          type: ACTIONS.ADD_STRING,
          payload: {
            id: modalWordToCopyState.id,
            key: modalWordToCopyState.wordTo,
          },
        }),
    },
    {
      name: "alternatives",
      checked: modalCheckboxState.alternatives,
      checkbox: {
        id: "alternatives",
        input: {
          " Alternatives": (
            <input
              name="Alternatives"
              type="checkbox"
              id="checkAlternatives"
              onChange={() =>
                setModalCheckboxState((prev) => ({
                  ...prev,
                  alternatives: !prev.alternatives,
                }))
              }
              checked={modalCheckboxState.alternatives}
            ></input>
          ),
        },
      },
      func: () =>
        dispatch({
          type: ACTIONS.SET_ALTERNATIVE,
          payload: {
            id: modalWordToCopyState.id,
            key: modalWordToCopyState.wordTo,
            value: modalWordToCopyState.alternatives,
          },
        }),
    },
    {
      name: "variableName",
      checked: modalCheckboxState.variableName,
      checkbox: {
        id: "variableName",
        input: {
          " Variable name": (
            <input
              name="variableName"
              type="checkbox"
              id="checkVariableName"
              onChange={() =>
                setModalCheckboxState((prev) => ({
                  ...prev,
                  variableName: !prev.variableName,
                }))
              }
              checked={modalCheckboxState.variableName}
            ></input>
          ),
        },
      },
      func: () =>
        dispatch({
          type: ACTIONS.SET_VARIABLE_NAME,
          payload: {
            id: modalWordToCopyState.id,
            key: modalWordToCopyState.wordTo,
            value: modalWordToCopyState.variableName,
          },
        }),
    },
    {
      name: "crossReference",
      checked: modalCheckboxState.crossReference,
      checkbox: {
        id: "crossReference",
        input: {
          // ➡️
          " Cross reference": (
            <input
              name="crossReference"
              type="checkbox"
              id="checkCrossReference"
              onChange={() =>
                setModalCheckboxState((prev) => ({
                  ...prev,
                  crossReference: !prev.crossReference,
                }))
              }
              checked={modalCheckboxState.crossReference}
            ></input>
          ),
        },
      },
      func: () =>
        dispatch({
          type: ACTIONS.SET_CROSS_REFERENCE,
          payload: {
            id: modalWordToCopyState.id,
            key: modalWordToCopyState.wordTo,
            value: modalWordToCopyState.crossReference,
          },
        }),
    },
    {
      name: "ruleReference",
      checked: modalCheckboxState.ruleReference,
      checkbox: {
        id: "ruleReference",
        input: {
          " Rule reference": (
            <input
              name="ruleReference"
              type="checkbox"
              id="checkRuleReference"
              onChange={() =>
                setModalCheckboxState((prev) => ({
                  ...prev,
                  ruleReference: !prev.ruleReference,
                }))
              }
              checked={modalCheckboxState.ruleReference}
            ></input>
          ),
        },
      },
      func: () =>
        dispatch({
          type: ACTIONS.SET_RULE_REFERENCE,
          payload: {
            id: modalWordToCopyState.id,
            key: modalWordToCopyState.wordTo,
            value: modalWordToCopyState.ruleReference,
          },
        }),
    },
    {
      name: "cardinality",
      checked: modalCheckboxState.cardinality,
      checkbox: {
        id: "cardinality",
        input: {
          " Cardinality": (
            <input
              name="cardinality"
              type="checkbox"
              id="checkCardinality"
              onChange={() =>
                setModalCheckboxState((prev) => ({
                  ...prev,
                  cardinality: !prev.cardinality,
                }))
              }
              checked={modalCheckboxState.cardinality}
            ></input>
          ),
        },
      },
      func: () =>
        dispatch({
          type: ACTIONS.SET_CARDINALITY,
          payload: {
            id: modalWordToCopyState.id,
            key: modalWordToCopyState.wordTo,
            value: modalWordToCopyState.cardinality,
          },
        }),
    },
    // Add validations
    ...modalWordToCopyState.validations.map(function (validation) {
      return createValidationCheckboxData(validation);
    }),
  ];

  function createValidationCheckboxData(validation) {
    const validationName = ` Validation ${validation.name}`;
    return {
      name: `Validation ${validation.name}`,
      checked: modalCheckboxState[validation.name] || false,
      checkbox: {
        id: validation.name,
        input: {
          [validationName]: (
            <input
              name="validation"
              type="checkbox"
              id={"check" + validation.name}
              onChange={() =>
                setModalCheckboxState((prev) => ({
                  ...prev,
                  [validation.name]: !prev[validation.name],
                }))
              }
              checked={modalCheckboxState[validation.name] || false}
            ></input>
          ),
        },
      },
      func: () =>
        dispatch({
          type: ACTIONS.ADD_VALIDATION,
          payload: {
            id: modalWordToCopyState.id,
            key: modalWordToCopyState.wordTo,
            value: validation.name,
            fieldsObj: validation?.fields,
          },
        }),
    };
  }

  function submit() {
    handleClose();
    addProperties();
    resetModalCheckboxState();
  }

  function getAllWordCopyPropertiesTrue() {
    const validationNames = [];
    for (const validation of modalWordToCopyState.validations) {
      validationNames.push(`Validation ${validation.name}`);
    }

    const filteredModalWordToCopyState = Object.entries(
      modalWordToCopyState
    ).filter(
      (allData) =>
        !(
          allData[0] === "id" ||
          allData[0] === "wordFrom" ||
          allData[0] === "wordTo" ||
          allData[0] === "validations"
        )
    );

    const propertyNames = [];

    for (const value of Object.values(filteredModalWordToCopyState)) {
      const propertyName = value[0];
      const propertyValue = value[1];
      if (propertyValue) {
        propertyNames.push(propertyName);
      }
    }

    return propertyNames.concat(validationNames);
  }

  function addProperties() {
    for (const state of checkboxData) {
      if (state.checked) {
        state.func();
      }
    }
  }

  function getCheckboxDataInputForPropertiesThatCanBeCopiedFromWord() {
    return getAllWordCopyPropertiesTrue().map(
      (property) =>
        checkboxData.find((val) => val.name === property).checkbox.input
    );
  }

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Copy properties from '{modalWordToCopyState.wordFrom}' to '
          {modalWordToCopyState.wordTo}'</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ textAlign: "left"}}>
          <div className="form" >
            {getCheckboxDataInputForPropertiesThatCanBeCopiedFromWord().map(
              (checkboxInput) =>
                Object.entries(checkboxInput).map((textAndInput, index) => (
                  <div key={index}>
                    {textAndInput[1]}{textAndInput[0]} <br />
                  </div>
                ))
            )}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              submit();
            }}
          >
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default PropertyCopyModal;
