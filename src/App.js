import React, { useReducer, useEffect, useState } from "react";
import { isIdentifier, isKeyword, rulenameExists } from "./AppStateUtil.js";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import "./interact/Interact.css";
import "./interact/Interact";

import Editor from "./components/editor/Editor";
import Sidebar from "./Sidebar.js";
import PropertyCopyModal from "./PropertyCopyModal.js";

export const CARDINALITY = {
  ONE: "1",
  ZERO_OR_ONE: "0..1",
  ZERO_OR_MORE: "0..N",
  ONE_OR_MORE: "1..N",
};

export const ACTIONS = {
  ADD_KEYWORD: "add-keyword",
  REMOVE_KEYWORD: "remove-keyword",
  ADD_IDENTIFIER: "add-identifier",
  REMOVE_IDENTIFIER: "remove-identifier",
  ADD_NUMBER: "add-number",
  REMOVE_NUMBER: "remove-number",
  ADD_STRING: "add-string",
  REMOVE_STRING: "remove-string",
  SET_ALTERNATIVE: "set-alternative",
  REMOVE_ALTERNATIVE: "remove-alternative",
  SET_CROSS_REFERENCE: "set-cross-reference",
  REMOVE_CROSS_REFERENCE: "remove-cross-reference",
  SET_RULE_REFERENCE: "set-rule-reference",
  REMOVE_RULE_REFERENCE: "remove-rule-reference",
  REMOVE_ALL_RULE_REFERENCES_TO_EDITOR: "remove-all-rule-references-to-editor",
  SET_VARIABLE_NAME: "set-variable-name",
  SET_CARDINALITY: "set-cardinality",
  ADD_VALIDATION: "add-validation",
  REMOVE_VALIDATION: "remove-validation",
  INCREMENT_COUNT: "increment-count",
  ADD_EDITOR: "add-editor",
  SET_STATE: "set-state",
  REMOVE_EDITOR: "remove-editor",
  RESET_STATE: "reset-state",
  UPDATE_EDITOR_PARENT: "update-editor-parent",
  UPDATE_EDITOR_RULENAME: "update-editor-rulename",
  UPDATE_EDITOR_CODE: "update-editor-code",
  SET_EDITOR_VISIBILITY: "set-editor-visibility",
  SET_EDITOR_IS_ROOT: "set-editor-is-root",
};

export function createNewTokenKeyword(word) {
  return { ...createNewToken(word), isKeyword: true };
}

export function createNewToken(word) {
  return {
    word: word,
    isKeyword: false,
    isIdentifier: false,
    isNumber: false,
    isString: false,
    alternatives: "",
    crossReference: "",
    variableName: "",
    cardinality: "",
    ruleReference: "",
    validations: [],
  };
}

function reducer(state, action) {
  const { type, payload } = action;

  switch (type) {
    case ACTIONS.ADD_KEYWORD: {
      const edState = getEditorState();
      if (isIdentifier(edState, payload.key)) {
        const type = "warning";
        const title = "Identifier can not be keyword";
        const message =
          `${payload.key} is an identifier. Identifiers can not be keywords.`;
        window.ipcRenderer.send(
          "electron-dialog",
          type,
          title,
          message
        );
        return { ...state };
      }
      updateToken("isKeyword", true);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.REMOVE_KEYWORD: {
      updateToken("isKeyword", false);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.ADD_IDENTIFIER: {
      const edState = getEditorState();
      if (isKeyword(edState, payload.key)) {
        const type = "warning";
        const title = "Keyword can not be identifier";
        const message =
          `${payload.key} is a keyword. Keywords can not be identifiers.`;
        window.ipcRenderer.send(
          "electron-dialog",
          type,
          title,
          message
        );
        return { ...state };
      }
      updateToken("isIdentifier", true);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.REMOVE_IDENTIFIER: {
      updateToken("isIdentifier", false);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.ADD_NUMBER: {
      updateToken("isNumber", true);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.REMOVE_NUMBER: {
      updateToken("isNumber", false);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.ADD_STRING: {
      updateToken("isString", true);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.REMOVE_STRING: {
      updateToken("isString", false);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.SET_ALTERNATIVE: {
      const alternativesAsArray = payload.value.split("|");
      if (!alternativesAsArray.includes(payload.key)) {
        console.error(
          `${payload.key} is not in alternatives [${alternativesAsArray}]`
        );
        const type = "warning";
        const title = "Alternatives";
        const message = `${payload.key} is not in alternatives [${alternativesAsArray}]`;
        window.ipcRenderer.send("electron-dialog", type, title, message);
        return { ...state };
      }
      updateToken("alternatives", payload.value);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.REMOVE_ALTERNATIVE: {
      updateToken("alternatives", "");
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.SET_CROSS_REFERENCE: {
      updateToken("crossReference", payload.value);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.REMOVE_CROSS_REFERENCE: {
      updateToken("crossReference", "");
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.SET_RULE_REFERENCE: {
      updateToken("ruleReference", payload.value);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.REMOVE_RULE_REFERENCE: {
      updateToken("ruleReference", "");
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.REMOVE_ALL_RULE_REFERENCES_TO_EDITOR: {
      const allEditorStates = getAllEditorStates();
      const editorID = payload.id;
      for (const edState of Object.values(allEditorStates)) {
        for (const token of edState.tokens) {
          // Use == because ruleReference is a string and editorID is a number.
          if (token.ruleReference == editorID) {
            payload.id = edState.id;
            payload.key = token.word;
            updateToken("ruleReference", "");
          }
          // Use == because ruleReference is a string and editorID is a number.
          if (token.crossReference == editorID) {
            payload.id = edState.id;
            payload.key = token.word;
            updateToken("crossReference", "");
          }
        }
      }
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.SET_VARIABLE_NAME: {
      updateToken("variableName", payload.value);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.SET_CARDINALITY: {
      updateToken("cardinality", payload.value);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.ADD_VALIDATION: {
      //payload.id: editor (getEditorState() uses id to find the correct state)
      //payload.key: word (used in getToken(edState) to find the correct place to update)

      const validationName = payload.value;
      const fieldsObj = payload.fieldsObj;

      const edState = getEditorState();
      let token = getToken(edState);
      let validations = token.validations;
      if (validations.some((val) => val.name === validationName)) {
        console.log("Validation already exists.");
        return { ...state };
      }

      console.log(`Adding new validation: ${validationName}.`);
      let newValidation = { name: validationName };

      if (fieldsObj !== null) {
        newValidation = { ...newValidation, fields: fieldsObj };
      }

      validations.push(newValidation);
      updateToken("validations", validations);

      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.REMOVE_VALIDATION: {
      //payload.id: editor
      const word = payload.key;
      const validationName = payload.value;

      // Find validation
      const edState = getEditorState();
      let token = getToken(edState);
      let validations = token.validations;

      // Filter out validation that we want to remove
      validations = validations.filter(
        (validation) => validation.name !== validationName
      );

      console.log(`Removing validation: ${validationName}`);
      // Insert other validations back into state
      updateToken("validations", validations);
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.INCREMENT_COUNT: {
      return { ...state, count: state.count + 1 };
    }
    case ACTIONS.ADD_EDITOR: {
      const newEditor = {
        id: state.count,
        languageName: "test" + state.count,
        rulename: "Rulename" + state.count,
        isVisible: true,
      };
      const newEditorState = {
        key: state.count,
        id: state.count,
        code: "",
        parentID: -1,
        isRoot: false,
        tokens: [],
      };
      return {
        ...state,
        editors: { ...state.editors, [state.count]: newEditor },
        editorsState: { ...state.editorsState, [state.count]: newEditorState },
      };
    }
    case ACTIONS.SET_STATE: {
      // Use when importing a project
      const newState = payload.value;
      return { ...newState };
    }
    case ACTIONS.REMOVE_EDITOR: {
      delete state.editors[payload.id];
      delete state.editorsState[payload.id];
      return {
        ...state,
      };
    }
    case ACTIONS.RESET_STATE: {
      return { count: 0, editors: {}, editorsState: {} };
    }
    case ACTIONS.UPDATE_EDITOR_PARENT: {
      const edState = getEditorState();
      edState.parentID = payload.value;
      return { ...state, editorsState: { ...state.editorsState } };
    }
    case ACTIONS.UPDATE_EDITOR_RULENAME: {
      const ed = getEditor();
      const newEditorRulename = payload.value;

      // Check that rulename starts with an upper case letter and that it doesn't already exist.
      if (
        newEditorRulename[0].toUpperCase() !== newEditorRulename[0] ||
        rulenameExists(state.editors, newEditorRulename, payload.id)
      ) {
        return { ...state };
      } else {
        ed.rulename = payload.value;
        return { ...state, editors: { ...state.editors } };
      }
    }
    case ACTIONS.UPDATE_EDITOR_CODE: {
      const edState = getEditorState();
      return {
        ...state,
        editorsState: {
          ...state.editorsState,
          [payload.id]: { ...edState, code: payload.value },
        },
      };
    }
    case ACTIONS.SET_EDITOR_VISIBILITY: {
      const ed = getEditor();
      ed.isVisible = payload.value;
      return { ...state };
    }
    case ACTIONS.SET_EDITOR_IS_ROOT: {
      const edState = getEditorState();
      //payload id: current editor
      //payload.value: true or false
      let bool = payload.value;
      edState.isRoot = bool;
      return { ...state, editorsState: { ...state.editorsState } };
    }
    default:
      return state;
  }

  function updateToken(property, value) {
    const edState = getEditorState();
    let token = getToken(edState);
    token = { ...token, [property]: value };
    const tokenIndex = findTokenIndex(edState);
    if (tokenIndex < 0) {
      // Token doesn't already exist. Add token to tokens array.
      edState.tokens.push(token);
    } else {
      // Update already existing token.
      edState.tokens[tokenIndex] = token;
    }
  }

  function getToken(edState) {
    let token = findToken(edState);
    if (!token) {
      token = { ...createNewToken(payload.key) };
    }
    return token;
  }

  /**
   *
   * @param {*} edState
   * @returns index in tokens array if it exists, -1 otherwise.
   */
  function findTokenIndex(edState) {
    return edState?.tokens.findIndex(({ word }) => word === payload.key);
  }

  function findToken(edState) {
    return edState?.tokens.find(({ word }) => word === payload.key);
  }

  function getEditor() {
    return state.editors[payload.id];
  }

  function getEditorState() {
    return state.editorsState[payload.id];
  }

  function getAllEditorStates() {
    return state.editorsState;
  }
}

export default function App() {
  const initialState = {
    count: 0,
    editors: {},
    editorsState: {},
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  // When dragging a word to another word, the state of the first word is copied so that it can be given to the second word.
  const [modalWordToCopyState, setModalWordToCopyState] = useState({
    id: 1,
    wordFrom: "",
    wordTo: "",
    keyword: false,
    identifier: false,
    number: false,
    string: false,
    alternatives: "",
    variableName: "",
    crossReference: "",
    ruleReference: "",
    cardinality: "",
    validations: [],
  });

  const [showDragDropModal, setShowDragDropModal] = useState(false);
  const handleCloseDragDropModal = () => setShowDragDropModal(false);
  const handleShowDragDropModal = () => setShowDragDropModal(true);

  const [showAllKeywords, setShowAllKeywords] = useState(true);
  const [showAllIdentifiers, setShowAllIdentifiers] = useState(true);
  const [showAllNumbers, setShowAllNumbers] = useState(true);
  const [showAllStrings, setShowAllStrings] = useState(true);
  const [showAllAlternatives, setShowAllAlternatives] = useState(true);
  const [showAllVariableNames, setShowAllVariableNames] = useState(true);
  const [showAllCrossReferences, setShowAllCrossReferences] = useState(true);
  const [showAllRuleReferences, setShowAllRuleReferences] = useState(true);
  const [showAllCardinalities, setShowAllCardinalities] = useState(true);
  const [showAllValidations, setShowAllValidations] = useState(true);

  const toggleShowAllKeywords = () => setShowAllKeywords(!showAllKeywords);
  const toggleShowAllIdentifiers = () =>
    setShowAllIdentifiers(!showAllIdentifiers);
  const toggleShowAllNumbers = () => setShowAllNumbers(!showAllNumbers);
  const toggleShowAllStrings = () => setShowAllStrings(!showAllStrings);
  const toggleShowAllAlternatives = () =>
    setShowAllAlternatives(!showAllAlternatives);
  const toggleShowAllVariableNames = () =>
    setShowAllVariableNames(!showAllVariableNames);
  const toggleShowAllCrossReferences = () =>
    setShowAllCrossReferences(!showAllCrossReferences);
  const toggleShowAllRuleReferences = () =>
    setShowAllRuleReferences(!showAllRuleReferences);
  const toggleShowAllCardinalities = () =>
    setShowAllCardinalities(!showAllCardinalities);
  const toggleShowAllValidations = () =>
    setShowAllValidations(!showAllValidations);

  const checkboxLayersDataForAllEditors = [
    {
      label: "Keywords",
      value: showAllKeywords,
      onChange: toggleShowAllKeywords,
    },
    {
      label: "Identifiers",
      value: showAllIdentifiers,
      onChange: toggleShowAllIdentifiers,
    },
    {
      label: "Numbers",
      value: showAllNumbers,
      onChange: toggleShowAllNumbers,
    },
    {
      label: "Strings",
      value: showAllStrings,
      onChange: toggleShowAllStrings,
    },
    {
      label: "Alternatives",
      value: showAllAlternatives,
      onChange: toggleShowAllAlternatives,
    },
    {
      label: "Variable names",
      value: showAllVariableNames,
      onChange: toggleShowAllVariableNames,
    },
    {
      label: "CrossRef",
      value: showAllCrossReferences,
      onChange: toggleShowAllCrossReferences,
    },
    {
      label: "RuleRef",
      value: showAllRuleReferences,
      onChange: toggleShowAllRuleReferences,
    },
    {
      label: "Cardinalities",
      value: showAllCardinalities,
      onChange: toggleShowAllCardinalities,
    },
    {
      label: "Validations",
      value: showAllValidations,
      onChange: toggleShowAllValidations,
    },
  ];

  // If a layer here is true, the corresponding layer in each editor should be true as well.
  const editorMasterLayers = {
    showAllKeywords,
    showAllIdentifiers,
    showAllNumbers,
    showAllStrings,
    showAllAlternatives,
    showAllVariableNames,
    showAllCrossReferences,
    showAllRuleReferences,
    showAllCardinalities,
    showAllValidations,
  };

  function getRules() {
    let rules = [];
    for (const edState of Object.values(state.editorsState)) {
      const code = edState.code.replace(/[\r\n]+/g," ").split(" ");
      let tokens = [];
      for (const word of code) {
        const tokenToInclude = edState.tokens.find(
          (token) => token.word === word
        );
        if (tokenToInclude !== undefined) {
          tokens = [...tokens, tokenToInclude];
        }
      }
      edState.tokens = tokens;
      rules = [...rules, edState];
    }
    return rules;
  }

  useEffect(() => {
    console.log(state.editorsState);
  }, [state.editorsState]);

  useEffect(() => {
    window.editors = [];
    const coll = document.getElementsByClassName("collapsible");
    let i;

    for (i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function () {
        this.classList.toggle("active");
        const content = this.nextElementSibling;
        if (content.style.display === "block") {
          content.style.display = "none";
        } else {
          content.style.display = "block";
        }
      });
    }
  }, []);

  function newEditor(editor, edState) {
    return (
      <div key={editor.id} className="editor">
        {editor.isVisible ? (
          <Editor
            id={editor.id}
            languageName={editor.languageName}
            rulename={editor.rulename}
            state={edState}
            dispatch={dispatch}
            getAllEditorsInfo={getAllEditorsInfo}
            editorMasterLayers={editorMasterLayers}
            handleShowModal={handleShowDragDropModal}
            setModalWordToCopyState={setModalWordToCopyState}
            getEditorRulename={getEditorRulename}
          />
        ) : null}
      </div>
    );
  }

  function getAllEditorsInfo() {
    return state.editors;
  }

  function getEditorRulename(id) {
    return Object.values(state.editors).find((obj) => obj.id == id)?.rulename;
  }

  return (
    <div className="App">
      <div
        className="column"
        id="app-screen"
        style={{
          minWidth: window.screen.availWidth + "px",
          minHeight: window.screen.availHeight + "px",
          border: "1px solid black",
        }}
      >
        <PropertyCopyModal
          show={showDragDropModal}
          handleClose={handleCloseDragDropModal}
          modalWordToCopyState={modalWordToCopyState}
          dispatch={dispatch}
        ></PropertyCopyModal>
        <Sidebar
          getRules={getRules}
          checkboxData={checkboxLayersDataForAllEditors}
          state={state}
          dispatch={dispatch}
        ></Sidebar>
        {Object.keys(state.editors).map((editorID) => {
          const editor = state.editors[editorID];
          const edState = state.editorsState[editorID];
          return newEditor(editor, edState);
        })}
      </div>
    </div>
  );
}
