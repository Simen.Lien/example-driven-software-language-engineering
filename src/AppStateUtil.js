export function isKeyword(editorState, word) {
  if (!editorState) {
    return false;
  }
  return editorState?.tokens?.find((token) => token?.word === word)?.isKeyword;
}

export function isIdentifier(editorState, word) {
  if (!editorState) {
    return false;
  }
  return editorState?.tokens?.find((token) => token?.word === word)
    ?.isIdentifier;
}

export function isNumber(editorState, word) {
  if (!editorState) {
    return false;
  }
  return editorState?.tokens?.find((token) => token?.word === word)?.isNumber;
}

export function isString(editorState, word) {
  if (!editorState) {
    return false;
  }
  return editorState?.tokens?.find((token) => token?.word === word)?.isString;
}

export function getAlternatives(editorState, word) {
  if (!editorState) {
    return;
  }
  return editorState?.tokens?.find((token) => token?.word === word)
    ?.alternatives;
}

export function getVariableName(editorState, word) {
  if (!editorState) {
    return;
  }
  return editorState?.tokens?.find((token) => token?.word === word)
    ?.variableName;
}

export function getCrossReference(editorState, word) {
  if (!editorState) {
    return;
  }
  return editorState?.tokens?.find((token) => token?.word === word)
    ?.crossReference;
}

export function getRuleReference(editorState, word) {
  if (!editorState) {
    return;
  }
  return editorState?.tokens?.find((token) => token?.word === word)
    ?.ruleReference;
}

export function getCardinality(editorState, word) {
  if (!editorState) {
    return;
  }
  return editorState?.tokens?.find((token) => token?.word === word)
    ?.cardinality;
}

export function getValidations(editorState, word) {
  if (!editorState) {
    return false;
  }
  return editorState?.tokens?.find((token) => token?.word === word)?.validations;
}

export function isValidation(editorState, word, validationName) {
  if (!editorState) {
    return false;
  }
  const token = editorState?.tokens?.find((token) => token?.word === word);
  return token?.validations.some((validation) => validation?.name === validationName);
}

// Returns true if @rulename already exists, false if not.
export function rulenameExists(editors, rulename, excludeEditorID){
  for (const editor of Object.values(editors)) {
    if(editor.id === excludeEditorID){
      continue;
    }
    const existingEditorRulename = editor.rulename;
    if (rulename === existingEditorRulename) {
      //console.error(`Rulename: ${rulename} already exists!`);
      return true;
    }
  }
  return false;
}