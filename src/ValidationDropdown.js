import React from "react";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";

const ValidationDropdown = ({
  title,
  validationData,
  setValidationText,
  isWordNumber,
}) => {
  return (
    <div className="dropdown">
      <DropdownButton
        id="dropdown-item-button"
        variant={"primary"}
        title={title}
        style={{ paddingLeft: "8px" }}
      >
        {validationData.map((validation, index) =>
          DropdownItem(index, setValidationText, validation)
          /*isWordNumber && validation.validationForNumber
            ? DropdownItem(index, setValidationText, validation)
            : (!isWordNumber && !validation.validationForNumber
            ? DropdownItem(index, setValidationText, validation)
            : null)*/
        )}
      </DropdownButton>
    </div>
  );
};

export default ValidationDropdown;
function DropdownItem(index, setValidationText, validation) {
  return (
    <Dropdown.Item
      key={index}
      onClick={() => {
        setValidationText(validation?.name);
      }}
    >
      {validation?.name}
    </Dropdown.Item>
  );
}
