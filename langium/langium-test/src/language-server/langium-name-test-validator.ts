
          import { ValidationAcceptor, ValidationCheck, ValidationRegistry } from 'langium'; 
          
          import {  
          Ingredient,
      StepAddIngredientAmountToTool,
       LangiumNameTestAstType } from './generated/ast'; 
          
          import { LangiumNameTestServices } from './langium-name-test-module'; 
          
          /** 
          
          * Map AST node types to validation checks. 
          
          */ 
          
          type LangiumNameTestChecks = { [type in LangiumNameTestAstType]?: ValidationCheck | ValidationCheck[] } 
          
                  export class LangiumNameTestValidationRegistry extends ValidationRegistry { 
                  constructor(services: LangiumNameTestServices) { 
                  super(services); 
                  const validator = services.validation.LangiumNameTestValidator; 
                  const checks: LangiumNameTestChecks = { 
                  
                  
                  
                  Ingredient: [
                          
                              validator.checkIngredientNameStartWithLowerCase, 
                              validator.checkIngredientAmountValueGreaterThanOrEqualTo

                          ]
                      
                      ,
                  StepAddIngredientAmountToTool: validator.checkStepAddIngredientAmountToToolAmountValueLessThanOrEqualTo
                      ,
                  
                  
                  
                  
                  }; 
                  this.register(checks, validator); 
                  } 
                  } 
              
              export class LangiumNameTestValidator { checkIngredientNameStartWithLowerCase(ingredient: Ingredient, accept: ValidationAcceptor): void { 
                  if(ingredient.name){ 
                      const firstChar = ingredient.name.substring(0, 1); 
                      if(firstChar.toLowerCase() !== firstChar){ 
                          accept('warning', 'Ingredient name should start with a lower case letter.', { node: ingredient, property: 'name' }); 
                      } 
                  } 
              } checkIngredientAmountValueGreaterThanOrEqualTo(ingredient: Ingredient, accept: ValidationAcceptor): void {
      if(ingredient.amount){
          const amount = ingredient.amount;
          if(!(amount >= 5)){
              accept('warning', 'Ingredient amount should be greater than or equal to 5.', { node: ingredient, property: 'amount' });
          }
      }
  }
checkStepAddIngredientAmountToToolAmountValueLessThanOrEqualTo(stepaddingredientamounttotool: StepAddIngredientAmountToTool, accept: ValidationAcceptor): void {
      if(stepaddingredientamounttotool.amount){
          const amount = stepaddingredientamounttotool.amount;
          if(amount > 5){
              accept('warning', 'StepAddIngredientAmountToTool amount should be less than or equal to 5.', { node: stepaddingredientamounttotool, property: 'amount' });
          }
      }
  }
  
          } 