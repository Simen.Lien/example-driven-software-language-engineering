"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LangiumNameTestValidator = exports.LangiumNameTestValidationRegistry = void 0;
const langium_1 = require("langium");
class LangiumNameTestValidationRegistry extends langium_1.ValidationRegistry {
    constructor(services) {
        super(services);
        const validator = services.validation.LangiumNameTestValidator;
        const checks = {
            Ingredient: [
                validator.checkIngredientNameStartWithLowerCase,
                validator.checkIngredientAmountValueGreaterThanOrEqualTo
            ],
            StepAddIngredientAmountToTool: validator.checkStepAddIngredientAmountToToolAmountValueLessThanOrEqualTo,
        };
        this.register(checks, validator);
    }
}
exports.LangiumNameTestValidationRegistry = LangiumNameTestValidationRegistry;
class LangiumNameTestValidator {
    checkIngredientNameStartWithLowerCase(ingredient, accept) {
        if (ingredient.name) {
            const firstChar = ingredient.name.substring(0, 1);
            if (firstChar.toLowerCase() !== firstChar) {
                accept('warning', 'Ingredient name should start with a lower case letter.', { node: ingredient, property: 'name' });
            }
        }
    }
    checkIngredientAmountValueGreaterThanOrEqualTo(ingredient, accept) {
        if (ingredient.amount) {
            const amount = ingredient.amount;
            if (!(amount >= 5)) {
                accept('warning', 'Ingredient amount should be greater than or equal to 5.', { node: ingredient, property: 'amount' });
            }
        }
    }
    checkStepAddIngredientAmountToToolAmountValueLessThanOrEqualTo(stepaddingredientamounttotool, accept) {
        if (stepaddingredientamounttotool.amount) {
            const amount = stepaddingredientamounttotool.amount;
            if (amount > 5) {
                accept('warning', 'StepAddIngredientAmountToTool amount should be less than or equal to 5.', { node: stepaddingredientamounttotool, property: 'amount' });
            }
        }
    }
}
exports.LangiumNameTestValidator = LangiumNameTestValidator;
//# sourceMappingURL=langium-name-test-validator.js.map