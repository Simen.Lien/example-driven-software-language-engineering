# Example-Driven-Software-Language-Engineering
An application for defining Domain-Specific Languages (DSLs) with high-quality IDE support in an Example-Driven way.


## Cloning project with HTTPS? ##
If getting an error: HTTP Basic: Access denied

Create an access token in your Gitlab profile by clicking on your profile picture in the top right corner -> preferences -> Access Tokens.
Then give your token a name, expiration date, and scopes, and create the token.
When trying to clone the project, one is prompted to give your username and password. Use this access token as the password.


## Prerequisites for running the project ##

* Install Node.js by going to https://nodejs.org/en/ (npm gets installed if you don't already have it)

* Install yarn by running 'npm install --global yarn'

* Install Electron by running 'yarn add --dev electron'



## Running the project ##
Start the project by running 'yarn electron:serve' at the root of the application.


### You might get an error saying "... cannot be loaded because running scripts is disabled on this system". ###
In this case, try running Visual Studio Code as an administrator, and running 'Set-ExecutionPolicy RemoteSigned -Scope CurrentUser' in the terminal.
When you're finished, you can run 'Set-ExecutionPolicy Restricted -Scope CurrentUser' to revert the execution policy if you would like to.
