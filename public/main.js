const { app, BrowserWindow, ipcMain } = require("electron");
const fs = require("fs");

const path = require("path");
const isDev = require("electron-is-dev");
const process = require("process");
const prompt = require("electron-prompt");
const { dialog } = require("electron");

require("@electron/remote/main").initialize();

const child_process = require("child_process");
const { Console } = require("console");

/*
 * This file is running Node. If you want other parts of the application (not running Node) to run
 * Node commands, you can do so by using the ipcMain and ipcRenderer concept explained here:
 * https://www.electronjs.org/docs/latest/api/ipc-main and
 * https://www.electronjs.org/docs/latest/api/ipc-renderer.
 */

// This function will output the lines from the script
// and will return the full combined output
// as well as exit code when it's done (using the callback).
function run_script(command, args, callback) {
  var child = child_process.spawn(command, args, {
    encoding: "utf8",
    shell: true,
  });
  // You can also use a variable to save the output for when the script closes later
  child.on("error", (error) => {
    console.log(error);
  });

  child.stdout.setEncoding("utf8");
  child.stdout.on("data", (data) => {
    //Here is the output
    data = data.toString();
    console.log(data);
  });

  child.stderr.setEncoding("utf8");
  child.stderr.on("data", (data) => {
    // Return some data to the renderer process with the mainprocess-response ID
    //mainWindow.webContents.send('mainprocess-response', data);
    //Here is the output from the command
    console.log(data);
  });

  child.on("close", (code) => {
    //Here you can get the exit code of the script
    switch (code) {
      case 0:
        console.log(code);
        break;
    }
  });
  if (typeof callback === "function") callback();
}

function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      preload: __dirname + "/preload.js",
      contextIsolation: false,
    },
  });

  win.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );
}

app.on("ready", function () {
  createWindow();
});

// Quit when all windows are closed.
app.on("window-all-closed", function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});

ipcMain.on("generate_langium_grammar", () => {
  const newPath = app.getAppPath() + "\\langium\\langium-test";
  process.chdir(newPath);

  run_script("npm run langium:generate", [], null);
  run_script("npm run build", [], null);
});

ipcMain.on("save_grammar", function (event, data) {
  const newPath =
    app.getAppPath() + "\\langium\\langium-test\\src\\language-server";
  process.chdir(newPath);

  console.log(data);
  fs.writeFile("langium-name-test.langium", data, function (err) {
    if (err) {
      return console.log(err);
    }
    console.log("Saving grammar succeeded");
  });
});

ipcMain.on("save_validations", function (event, data) {
  const newPath =
    app.getAppPath() + "\\langium\\langium-test\\src\\language-server";
  process.chdir(newPath);

  fs.writeFile("langium-name-test-validator.ts", data, function (err) {
    if (err) {
      return console.log(err);
    }
    console.log("Saving validations succeeded");
  });
});

ipcMain.on("save_generator.ts", function (event, data) {
  const newPath = app.getAppPath() + "\\langium\\langium-test\\src\\cli";
  process.chdir(newPath);

  fs.writeFile("generator.ts", data, function (err) {
    if (err) {
      return console.log(err);
    }
    console.log("Saving generator.ts succeeded");
  });
});

ipcMain.on("save_index.ts", function (event, data) {
  const newPath = app.getAppPath() + "\\langium\\langium-test\\src\\cli";
  process.chdir(newPath);

  fs.writeFile("index.ts", data, function (err) {
    if (err) {
      return console.log(err);
    }
    console.log("Saving index.ts succeeded");
  });
});

ipcMain.on("run_langium", () => {
  const newPath = app.getAppPath() + "\\langium\\langium-test";
  process.chdir(newPath);

  run_script("code .", [], null);
});

ipcMain.handle("electron-prompt-cardinality", async () => {
  return prompt({
    title: "Cardinality prompt",
    label: "Cardinality",

    selectOptions: {
      NONE: "None",
      ONE: "1",
      ONE_OR_MORE: "1..N (+)",
      ZERO_OR_ONE: "0..1 (?)",
      ZERO_OR_MORE: "0..N (*)",
    },
    type: "select",
  })
    .then((r) => {
      if (r === null) {
        return "cancel";
      } else {
        if (r === "ONE") {
          return "1";
        } else if (r === "ONE_OR_MORE") {
          return "1..N";
        } else if (r === "ZERO_OR_ONE") {
          return "0..1";
        } else if (r === "ZERO_OR_MORE") {
          return "0..N";
        } else {
          return "";
        }
      }
    })
    .catch(console.error);
});

ipcMain.handle("electron-prompt-alternatives", async () => {
  return prompt({
    title: "Alternatives prompt",
    label: "Enter different alternatives, separated by '|'",
    value: "ML|G|L",
    inputAttrs: {
      type: "text",
    },
    type: "input",
    width: 400,
    height: 200,
  });
});

ipcMain.handle("electron-prompt-variable-name", async () => {
  return prompt({
    title: "Variable name prompt",
    label: "Enter a variable name",
    value: "name",
    inputAttrs: {
      type: "text",
    },
    type: "input",
    width: 400,
    height: 200,
  });
});

ipcMain.handle(
  "electron-prompt-choose-rulename",
  async (event, title, editorRulenames) => {
    // Add "No reference" as an option so that the user is able to remove references.
    let allRulenames = { "": "--- No reference ---" };
    for (const rulename of editorRulenames) {
      allRulenames = { ...allRulenames, [rulename]: rulename };
    }
    return prompt({
      title: title,
      label: "Choose a rulename",
      selectOptions: {
        ...allRulenames,
      },
      type: "select",
    });
  }
);

ipcMain.handle(
  "electron-prompt-choose-parent",
  async (event, title, label, otherEditors) => {
    let allRulenames = { "": "--- No reference ---" };
    for (const [key, value] of Object.entries(otherEditors)) {
      allRulenames = { ...allRulenames, [key]: value.rulename };
    }

    // Return id of chosen editor rulename
    return prompt({
      title: title,
      label: label,
      width: 600,
      height: 200,
      selectOptions: {
        ...allRulenames,
      },
      type: "select",
    });
  }
);

ipcMain.on("electron-dialog", async (_, type, title, message) => {
  return dialog.showMessageBox({
    type: type,
    title: title,
    message: message,
  });
});

ipcMain.handle("electron-save-dialog", async (_, data) => {
  const defaultPath = app.getAppPath() + "\\src\\projects";
  const saveDialogResult = await dialog.showSaveDialog({
    title: "Save project",
    defaultPath: defaultPath,
    filters: [{ name: "Text (.txt)", extensions: ["txt"] }],
  });

  if (saveDialogResult.canceled) {
    return;
  }

  fs.writeFile(saveDialogResult.filePath, data, { flag: "w" }, function (err) {
    if (err) {
      return console.log(err);
    }
    console.log("Saving file succeeded");
  });
});

ipcMain.handle("electron-open-dialog", async (_) => {
  const defaultPath = app.getAppPath() + "\\src\\projects";
  const openDialogResult = await dialog.showOpenDialog({
    title: "Open project",
    defaultPath: defaultPath,
    filters: [{ name: "Text (.txt)", extensions: ["txt"] }],
  });

  let result = { canceled: openDialogResult.canceled, data: "" };

  if (openDialogResult.canceled) {
    return result;
  }
  const filepath = openDialogResult.filePaths[0];
  try {
    const data = fs.readFileSync(filepath, "utf8");
    result = { ...result, data: data };
    return result;
  } catch (err) {
    console.error(err);
  }
});

ipcMain.handle("electron-confirmation-dialog", async(_, options) => {
  return dialog.showMessageBox(options);
})
